package br.com.fiap.ecommerce.filter;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.filter.GenericFilterBean;

import br.com.fiap.uteis.TokenUteis;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.SignatureException;

public class LoginFilter extends GenericFilterBean{
	
	 @Override
	    public void doFilter(final ServletRequest req, final ServletResponse res,
	                         final FilterChain chain) throws IOException, ServletException {
	        final HttpServletRequest request = (HttpServletRequest) req;
	        final HttpServletResponse response = (HttpServletResponse) res;
	        
	        final String authHeader = request.getHeader("Authorization");
	        
	        if (authHeader == null) {
	        	response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
	            return; //break filter chain, requested JSP/servlet will not be executed
	        }

	        final String token = authHeader; 

	        try {
	            final Claims claims = TokenUteis.getClaims(token);
	            if (claims == null || !TokenUteis.isValido(token)) {
	            	response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
		            return; //break filter chain, requested JSP/servlet will not be executed
				}
	            request.setAttribute("claims", claims);
	        }
	        catch (final SignatureException e) {
	            throw new ServletException("Não Autorizado");
	        }

	        chain.doFilter(req, res);
	    }
}
