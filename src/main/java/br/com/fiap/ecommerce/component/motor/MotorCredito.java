package br.com.fiap.ecommerce.component.motor;

import java.io.IOException;
import java.util.List;

import org.apache.commons.mail.EmailException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import br.com.fiap.ecommerce.component.PedidoComponent;
import br.com.fiap.ecommerce.component.PessoaComponent;
import br.com.fiap.ecommerce.dto.FiltrosPedidoDTO;
import br.com.fiap.ecommerce.entity.Pedido;
import br.com.fiap.ecommerce.enums.StatusPedidoEnum;
import br.com.fiap.uteis.ConsistirException;
import br.com.fiap.uteis.EnviarEmail;

@Component
public class MotorCredito {

	@Autowired
	private PedidoComponent pedidoComponent;
	
	@Autowired
	private PessoaComponent pessoaComponent;
	
	/**
	 * A cada 30 minutos
	 */ 
	@Scheduled(cron="1 * * * * *") 
	private void analisarPedidosSolicitados(){
		FiltrosPedidoDTO filtros = new FiltrosPedidoDTO();
		filtros.setStatus(StatusPedidoEnum.SOLICITADO); 
		
		try {
			List<Pedido> pedidos = pedidoComponent.consultar(filtros, null);
			for (Pedido pedido : pedidos) {
				
				//2.Cliente está na blacklist;
				boolean clienteBloqueado = false;
				Integer idPessoa = pedido.getPessoa().getIdPessoa();
				
				clienteBloqueado = pessoaComponent.isPessoNaBlackList(idPessoa);

//				3.Tem dívida com a empresa;
				boolean possuiDivida = pedidoComponent.pessoaComPendencias(pedido.getPessoa());
			 
				
				if (clienteBloqueado || possuiDivida) {
					pedido.setSituacao(StatusPedidoEnum.REPROVADO);
					enviarEmailPedidoReprovado(pedido);
				} else {
					enviarEmaiPedidoAprovado(pedido);
					pedido.setSituacao(StatusPedidoEnum.AGUARDANDO);
				}
				pedidoComponent.atualizar(pedido);
			}
		} catch (ConsistirException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
 
 
	private void enviarEmailPedidoReprovado(Pedido pedido) {
		try {
			EnviarEmail.enviarEmailPedidoRegeitado(pedido);
			 
		} catch (EmailException e) { 
			e.printStackTrace();
		} 
		
	}

//	Caso seja aprovada a análise deve ser gerado um boleto com o valor total da compra, uma nota fiscal com cada
//	item, suas quantidades, seus valores unitários discriminados separadamente, e com todos os dados
//	do cliente que realizou a compra, além é claro, dos impostos calculados. 
	private void enviarEmaiPedidoAprovado(Pedido pedido) {
		try {
			pedidoComponent.gerarBoleto(pedido);
			EnviarEmail.enviarBoleto(pedido);
		 
		} catch (IOException e) { 
			e.printStackTrace();
		}  
		
	}
}
