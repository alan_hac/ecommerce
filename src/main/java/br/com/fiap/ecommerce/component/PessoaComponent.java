package br.com.fiap.ecommerce.component;

import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.fiap.ecommerce.dao.PessoaRepository;
import br.com.fiap.ecommerce.dto.FiltroPessoaDTO;
import br.com.fiap.ecommerce.entity.Endereco;
import br.com.fiap.ecommerce.entity.Pessoa;
import br.com.fiap.ecommerce.enums.TipoUsuarioEnum;
import br.com.fiap.uteis.ConsistirException;

@Component
public class PessoaComponent extends BaseCRUDComponent<Pessoa> {

	@Autowired
	private PessoaRepository pessoaRepository;
	
	@PostConstruct
	public void setUp() {
		setBaseRepository(pessoaRepository);
	}
	 
	@SuppressWarnings("unchecked")
	public List<Pessoa> buscarPorFiltros(FiltroPessoaDTO filtro) throws ConsistirException{

		Criteria criteria = entityManager.unwrap(Session.class).createCriteria(Pessoa.class, "pessoa");
		if (filtro !=null){
			if (StringUtils.isNotBlank(filtro.getNome())){
				criteria.add(Restrictions.eq("nome", filtro.getNome()));
			}
			if (StringUtils.isNotBlank(filtro.getDocumento())){
				criteria.add(Restrictions.eq("documento", filtro.getDocumento()));
			}
			if (StringUtils.isNotBlank(filtro.getEmail())){
				criteria.add(Restrictions.eq("email", filtro.getEmail()));
			}
	        criteria.setFirstResult(filtro.getOffSet());
	        criteria.setMaxResults(filtro.getTamanhoLista());
		}
		List<Pessoa> lista = (List<Pessoa>) criteria.list();
		
		if (lista == null){
			ConsistirException consistir = new ConsistirException();
			consistir.adicionarMensagem("Nao existe pessoas cadastrados"); 
			throw consistir;
		}
		return lista ;
	}
	
	protected void validarCadastro(Pessoa pessoa) throws ConsistirException {
		ConsistirException consistir = new ConsistirException();
		if (StringUtils.isBlank(pessoa.getNome())){
			consistir.adicionarMensagem("Informe o campo Nome");
		}
		if (StringUtils.isBlank(pessoa.getUsuario().getLogin() )){
			consistir.adicionarMensagem("Informe o campo E-mail");
		} 
		
		if (consistir.existeErros()){
			throw consistir;
		}
		
	}

	public void cadastrarCliente(Pessoa pessoa) throws ConsistirException {
		if (pessoa.getUsuario() !=null){
			pessoa.getUsuario().setTipoUsuario(TipoUsuarioEnum.CLIENTE);
		}
		for (Endereco endereco : pessoa.getEnderecos()){
			endereco.setPessoa(pessoa);
		}
		super.persistir(pessoa);
	}

	public Pessoa carregarCliente(Integer idUsuario) throws ConsistirException {
		Pessoa pessoa = null;
		try{
		    pessoa = pessoaRepository.buscarPessoaPorUsuario(idUsuario);	
		    
		if (pessoa !=null && pessoa.getEnderecos() !=null){
			 Hibernate.initialize(pessoa.getEnderecos());
			 System.out.println(pessoa.getEnderecos());
		}
		 
		}catch(Exception e){
			e.printStackTrace();	
		}
		
		return pessoa;
	}

	public List<Endereco> buscarEnderecos(int idCliente) throws ConsistirException {
		try {
			return pessoaRepository.buscarEnderecosPorPessoa(idCliente);
		} catch (Exception e) {
			throw new ConsistirException("Erro ao obter os endereços");
		}
	}
	
	public Pessoa buscarUsuario(int idUsuario) throws ConsistirException {
		try {
			return pessoaRepository.buscarPessoaPorUsuario(idUsuario);
		} catch (Exception e) {
			throw new ConsistirException("Erro ao obter Usuario Logado");
		}
	}
	
	public boolean isPessoNaBlackList(Integer idPessoa){
		Pessoa pessoa = pessoaRepository.findOne(idPessoa);
		  
		return pessoa.isBlacklist() != null ? pessoa.isBlacklist() : false;
	}
}
