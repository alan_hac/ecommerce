package br.com.fiap.ecommerce.component;

import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.commons.lang.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

import br.com.fiap.ecommerce.dao.ProdutoRepository;
import br.com.fiap.ecommerce.dto.FiltroProdutosDTO;
import br.com.fiap.ecommerce.dto.ProdutoDTO;
import br.com.fiap.ecommerce.entity.Produto;
import br.com.fiap.uteis.ConsistirException;

/**
 * Componente que irá conter as Regras de negócio e chamadas para o Repository quando necessário fazer uma query.
 * 
 * @author Paulo Porto
 *
 */
@Component
public class ProdutoComponent extends BaseCRUDComponent<Produto> {
    
	@Autowired
	private ProdutoRepository produtoRepositoy;
	
	@PostConstruct
	public void setUp() {
		setBaseRepository(produtoRepositoy);
	}
	
	@SuppressWarnings("unchecked")
	public List<Produto> buscarPorFiltros(FiltroProdutosDTO filtro) throws ConsistirException{ 

		Criteria criteria = entityManager.unwrap(Session.class).createCriteria(Produto.class, "produto");
		if (filtro !=null){
			if (StringUtils.isNotBlank(filtro.getNome())){
				criteria.add(Restrictions.like("nome", filtro.getNome()+"%"));
			}
			if (StringUtils.isNotBlank(filtro.getFonecedor())){
				criteria.add(Restrictions.like("fonecedor", filtro.getFonecedor()+"%"));
			}
			if (StringUtils.isNotBlank(filtro.getNcm())){
				criteria.add(Restrictions.like("ncm", filtro.getNcm()+"%"));
			} 
			if (filtro.getPrecoInicio() !=null && filtro.getPrecoFim() != null){
				criteria.add(Restrictions.between("preco", filtro.getPrecoInicio(), filtro.getPrecoFim()));
			} else {
				if (filtro.getPrecoInicio() !=null){
					criteria.add(Restrictions.ge("preco", filtro.getPrecoInicio()));
				} else if (filtro.getPrecoFim() !=null){
					criteria.add(Restrictions.le("preco", filtro.getPrecoFim()));
				}
			} 
			if (filtro.getEstoqueInicio()!=null && filtro.getEstoqueFim()!= null){
				criteria.add(Restrictions.between("estoque", filtro.getEstoqueInicio(), filtro.getEstoqueFim()));
			} else {
				if (filtro.getEstoqueInicio() !=null){
					criteria.add(Restrictions.ge("estoque", filtro.getEstoqueInicio()));
				} else if (filtro.getEstoqueFim() !=null){
					criteria.add(Restrictions.le("estoque", filtro.getEstoqueFim()));
				}
			} 
			if (filtro.getCategoria() !=null){
				criteria.add(Restrictions.eq("categoria", filtro.getCategoria()));
			} 
			if (filtro.getDestaque() !=null){
				criteria.add(Restrictions.eq("destaque", filtro.getDestaque()));
			}  

	        criteria.setFirstResult(filtro.getOffSet());
	        criteria.setMaxResults(filtro.getTamanhoLista());
		}
		List<Produto> lista = (List<Produto>) criteria.list();
		
		if (lista == null){
			ConsistirException consistir = new ConsistirException();
			consistir.adicionarMensagem("Nao existe produtos cadastrados"); 
			throw consistir;
		}
		return lista ;
	}

	@Override
	protected void validarCadastro(Produto produto) throws ConsistirException {
		ConsistirException consistir = new ConsistirException();
		if (StringUtils.isBlank(produto.getDescricao())){
			consistir.adicionarMensagem("Informe a Descrição do Produto");
		}
		if (produto.getPreco() == null || produto.getPreco() <= 0L){
			consistir.adicionarMensagem("Informe um preço valido");
		}
		
		if (produto.getCategoria() == null || StringUtils.isBlank(produto.getCategoria().toString())){
			consistir.adicionarMensagem("Informe a Categoria");
		}
		
		if (produto.getPeso() == null || produto.getPeso() <= 0L){
			consistir.adicionarMensagem("Informe um peso valido");
		}
		if (produto.getEstoque() == null){
			consistir.adicionarMensagem("Informe uma quantidade disponivel");
		}
		if (consistir.existeErros()){
			throw consistir;
		}
	}
	
	public void validarCarrinho(List<ProdutoDTO> produtos) throws ConsistirException{
		for (ProdutoDTO produtoDTO : produtos) {
			Produto produto = this.buscarPorId(produtoDTO.getIdProduto());
			if (!possuiEstoque(produto, produtoDTO.getQuantidade())) {
				throw new ConsistirException("Quantidade Selecionada para o produto: "+ produto.getNome() +" Indisponivel em estoque! Limite: " + produto.getEstoque());
			}
			
			if (!produto.getPreco().equals(produtoDTO.getPreco())) {
				throw new ConsistirException("Os preços para o produto: " + produto.getNome() + " foram atualizados." );
			}
		}
	}

	/**
	 * TODO JAVADOC
	 * @param codigoProduto
	 * @param quantidadeProduto
	 * @return
	 */
	protected boolean possuiEstoque(Produto produto, int quantidadeProduto){
		if(produto != null & produto.getEstoque() >= quantidadeProduto){
			return true;
		}
		return false;
	}
	
	protected void baixaEstoque(Produto produto, Integer quantidade) {
		produto.setEstoque(produto.getEstoque() - quantidade);
		produtoRepositoy.save(produto);
	}

	@Cacheable("vitrine")
	public List<Produto> buscarProdutos() throws ConsistirException {
		try {
			Criteria criteria = entityManager.unwrap(Session.class).createCriteria(Produto.class, "produto");
			criteria.add(Restrictions.ge("estoque", 1));
			List<Produto> lista = (List<Produto>) criteria.list();
			return lista;
		} catch (Exception e) {
			throw new ConsistirException("Não Foi possivel buscar os produtos");
		}
		
		
		
	}
}
