package br.com.fiap.ecommerce.component;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.common.base.Strings;

import br.com.fiap.ecommerce.dao.UsuarioRepository;
import br.com.fiap.ecommerce.entity.Usuario;
import br.com.fiap.uteis.ConsistirException;
import br.com.fiap.uteis.TokenUteis;

/**
 * TODO : Javadoc
 * @author 
 *
 */
@Component
public class UsuarioComponent extends BaseCRUDComponent<Usuario> {

	@Autowired
	private UsuarioRepository usuarioRepository;

	@PostConstruct
	public void setUp() {
		setBaseRepository(usuarioRepository);
	}

	/**
	 * TODO: Javadoc
	 * @param login
	 * @param senha
	 * @return
	 * @throws ConsistirException
	 */
	public String validaLogin(String login, String senha) throws ConsistirException {
		String token = "";
		Usuario usuario =  usuarioRepository.buscarUsuarioPorLoginESenha(login, senha);
		
		if (usuario == null) {
			throw new ConsistirException("Usuário ou senha Inválido");
		}
		
		if (usuario != null && usuario.getSenha().equals(usuario.getSenha())) {
			token = TokenUteis.criaToken(usuario.getId(), usuario.getTipoUsuario().toString(), usuario.getLogin());
		} else {
			throw new ConsistirException("Usuário ou senha Inválido");
		}

		return token;
	}

	/**
	 * TODO: Implementar!
	 */
	@Override
	protected void validarCadastro(Usuario usuario) throws ConsistirException {
		if (Strings.isNullOrEmpty(usuario.getLogin())) {
			// TODO
		} else if (Strings.isNullOrEmpty(usuario.getSenha())) {
			// TODO
		}
		return;
	}

}
