package br.com.fiap.ecommerce.component;

import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.fiap.ecommerce.dao.EnderecoRepository;
import br.com.fiap.ecommerce.dao.PedidoRepository;
import br.com.fiap.ecommerce.dto.FiltrosPedidoDTO;
import br.com.fiap.ecommerce.dto.PedidoDTO;
import br.com.fiap.ecommerce.dto.ProdutoDTO;
import br.com.fiap.ecommerce.entity.Boleto;
import br.com.fiap.ecommerce.entity.Endereco;
import br.com.fiap.ecommerce.entity.Pedido;
import br.com.fiap.ecommerce.entity.PedidoProduto;
import br.com.fiap.ecommerce.entity.Pessoa;
import br.com.fiap.ecommerce.entity.Produto;
import br.com.fiap.ecommerce.enums.StatusPedidoEnum;
import br.com.fiap.uteis.ConsistirException;
import br.com.fiap.uteis.Uteis;

@Component
public class PedidoComponent extends BaseCRUDComponent<Pedido> {

	@Autowired
	private PedidoRepository pedidoRepository;
	
	@Autowired
	private ProdutoComponent produtoComponent;
	
	@Autowired
	private EnderecoRepository enderecoRepository;
	
	@PostConstruct
	public void setUp() {
		setBaseRepository(pedidoRepository);
	}
	
	public Pedido buscarPedido(Integer idPedido, Pessoa pessoa) throws ConsistirException{
		try {
			
			Pedido pedido = pedidoRepository.buscarPedidoPorPessoa(idPedido, pessoa.getIdPessoa());
			return pedido;
		} catch (Exception e) {
			throw new ConsistirException("Não foi possivel obter o pedido!");
		}
	}
	
	public Pedido confirmarPedido(PedidoDTO pedidoDTO, Pessoa pessoa) throws ConsistirException {
		
		Pedido pedido = new Pedido();
		
		pedido.setPessoa(pessoa);
		
		//consiste quantidade de produtos em estoque
		List<ProdutoDTO> produtos = pedidoDTO.getProdutos();
		for (ProdutoDTO produtoDTO : produtos) {
			Produto produto = produtoComponent.buscarPorId(produtoDTO.getIdProduto());
			
			this.validaEstoqueProduto(pedido, produto, produtoDTO.getQuantidade());
			
			PedidoProduto pedidoProduto = new PedidoProduto();
			pedidoProduto.setPedido(pedido);
			pedidoProduto.setProduto(produto);
			pedidoProduto.setQuantidade(produtoDTO.getQuantidade());
			//TODO Deve Realizar validação de valor
			pedidoProduto.setValor(produtoDTO.getPreco());
			pedido.addPedidoProduto(pedidoProduto);

		}
		Endereco enderecoEntrega = null;
		if(pedidoDTO.getEnderecoEntrega().isNew()){
			pedidoDTO.getEnderecoEntrega().setPessoa(pessoa);
			enderecoEntrega = enderecoRepository.save(pedidoDTO.getEnderecoEntrega());
		} else {
			enderecoEntrega = this.entityManager.merge(pedidoDTO.getEnderecoEntrega());
		}
		
		pedido.setEnderecoEntrega(enderecoEntrega);
		
		//Deixa Aguardando para caso de insucesso
		pedido.setSituacao(StatusPedidoEnum.SOLICITADO);
		pedido.setDataPedido(new Date());
		for (PedidoProduto pedidoProduto : pedido.getPedidoProdutos()) {
			produtoComponent.baixaEstoque(pedidoProduto.getProduto(), pedidoProduto.getQuantidade());
		}
		pedido.setValorTotal(pedidoDTO.getValorTotal());
		//Consiste pagamento
		pedido = this.persistir(pedido);
		
		return pedido;
	}

	private void validaEstoqueProduto(Pedido pedido, Produto produto, Integer quantidade) throws ConsistirException {
		boolean possuiEstoque = produtoComponent.possuiEstoque(produto, quantidade);
		if(!possuiEstoque){
			ConsistirException consistir = new ConsistirException();
			consistir.adicionarMensagem("Produto insuficientes no estoque : " + produto.getNome() ); 
			throw consistir;
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<Pedido> consultar(FiltrosPedidoDTO filtros, Pessoa pessoa) throws ConsistirException{
		Criteria criteria = entityManager.unwrap(Session.class).createCriteria(Pedido.class, "pedido");
		if (filtros != null){
			if (filtros.getDataInicio() !=null && filtros.getDataFim() != null){
				criteria.add(Restrictions.between("dataPedido", filtros.getDataInicio(), filtros.getDataFim()));
			} else {
				if (filtros.getDataInicio() !=null){
					criteria.add(Restrictions.ge("dataPedido", filtros.getDataInicio()));
				} else if (filtros.getDataFim() !=null){
					criteria.add(Restrictions.le("dataPedido", filtros.getDataFim()));
				}
			}
			if (pessoa != null && pessoa.getIdPessoa() != null && pessoa.getIdPessoa() != 0){
				criteria.createAlias("pedido.pessoa", "cliente");
				criteria.add(Restrictions.eq("cliente.id", pessoa.getIdPessoa()));
			}
			
			if (filtros.getStatus() != null){
				criteria.add(Restrictions.eq("situacao", filtros.getStatus()));
			} else {
				criteria.add(
						Restrictions.or(
						Restrictions.eq("situacao",StatusPedidoEnum.SOLICITADO),
						Restrictions.eq("situacao", StatusPedidoEnum.AGUARDANDO),
						Restrictions.eq("situacao", StatusPedidoEnum.APROVADO),
						Restrictions.eq("situacao", StatusPedidoEnum.REPROVADO)
						));
			}
		}
		List<Pedido> lista = criteria.list();
		
		if (lista == null){
			ConsistirException consistir = new ConsistirException();
			consistir.adicionarMensagem("Nao existe pedidos cadastrados"); 
			throw consistir;
		}
		return lista ;
	}
	
	@Override
    protected void validarCadastro(Pedido pedido) throws ConsistirException {
		ConsistirException consistir = new ConsistirException();
		if (pedido.getEnderecoEntrega() == null){
			consistir.adicionarMensagem("Selecione ou Cadastre um endereço para entrega");
		}
		
		if (consistir.existeErros()){
			throw consistir;
		} 
	}

	public void cancelarPedido(Integer idPedido, Pessoa usuarioLogado) throws ConsistirException {
		try {
			Pedido pedido = pedidoRepository.findOne(idPedido);
			pedido.setSituacao(StatusPedidoEnum.CANCELADO);
			this.atualizar(pedido);
		} catch (Exception e) {
			throw new ConsistirException(e.getMessage());
		}
	}
	

	
	public boolean pessoaComPendencias(Pessoa pessoa) throws ConsistirException{
		Criteria criteria = entityManager.unwrap(Session.class).createCriteria(Boleto.class, "boleto");
		criteria.createAlias("boleto.pedido", "pedido");
		criteria.createAlias("pedido.pessoa", "pessoa");
		criteria.add(Restrictions.eq("pedido.situacao", StatusPedidoEnum.AGUARDANDO));
		criteria.add(Restrictions.eq("pessoa.idPessoa", pessoa.getIdPessoa()));
		criteria.add(Restrictions.lt("datavencimento", new Date())); 
		
		Long qtdPedidosPendentes = (Long) criteria.setProjection(Projections.rowCount()).uniqueResult();
		boolean temPendencia = false;
		if (qtdPedidosPendentes != null && qtdPedidosPendentes > 0 ){
			temPendencia = true;
		}
		return temPendencia;
	}

	public void gerarBoleto(Pedido pedido) {
		Boleto boleto = new Boleto();
		boleto.setPedido(pedido);
		boleto.setIdpedido(pedido.getIdPedido());
		pedido.setBoleto(boleto); 
		boleto.setDatavencimento(Uteis.adicionarDiasEmData(new Date(), 7).getTime());
		boleto.setNumero_boleto(gerarNumeroBoleto());
		boleto.setValorboleto(pedido.getValorTotal());
	}
	
	private String gerarNumeroBoleto(){
 
		Criteria criteria = entityManager.unwrap(Session.class).createCriteria(Boleto.class, "boleto");
     	Long qtd = (Long) criteria.setProjection(Projections.rowCount()).uniqueResult();
		if (qtd == null){
			qtd = 0L;
		}
		Integer numeroBolero = qtd.intValue() + 1 ;
		return numeroBolero.toString();
	}
	
}
