package br.com.fiap.ecommerce.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.fiap.ecommerce.component.ProdutoComponent;
import br.com.fiap.ecommerce.entity.Produto;
import br.com.fiap.uteis.ConsistirException;

/**
 * Controller Criado para as funcionalidades referente ao Administrador, CRUD Produtos.
 * 
 * <b>Requisições</b>
 * Deve conter o Padrão RestFULL conforme descrito em {@link http://www.restapitutorial.com/lessons/restfulresourcenaming.html}
 * 
 * URL BASE: \api (Realiza a autenticação no <code>LoginFilter</code>)
 *  <b>URL</b>            <b>HTTP</b>      <b>Funcionalidade</b>
 * \produtos                 GET            listar todos os produtos
 * \produtos\:id             GET            Buscar um unico produto
 * \produtos                 POST           Salva um produto
 * \produtos                 PUT            Atualiza um produto  
 * 
 * <b>Status de Retorno</b>
 * 
 * HttpStatus.OK = 200 - Requisição bem sucedida contendo o json compativel com a requisição feita
 * HttpStatus.CREATED = 201 - Requisição bem sucedida referente a uma ação POST de criação, contendo 
 * HttpStatus.INTERNAL_SERVER_ERROR = 500 - Retorno contendo a lista de mensagens com o problema para poder ser tratado no front
 * 
 * obs: Não deve ser usado a URL desse controller para a Vitrine devido a necessidade de autenticação como ADMIN para as url's com /api
 * @author Paulo Porto
 *
 */
@RestController
@RequestMapping(value="/api")
public class ProdutoController extends SuperController  {
	static Logger log = Logger.getLogger(ProdutoController.class.getName());

	@Autowired
	private ProdutoComponent produtoComponent; 
	
	@RequestMapping(value="/produtos", method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)	
	public @ResponseBody ResponseEntity<Object> listar(){ 
		try { 
			List<Produto> produtos = produtoComponent.buscarTodos();			
			return new ResponseEntity<Object>(produtos, null, HttpStatus.OK);
		} catch (ConsistirException e) {
			return new ResponseEntity<Object>(e.getListaErros(), null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@RequestMapping(value="/produtos/{id}", method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)	
	public @ResponseBody ResponseEntity<Object> consultarPorId(@PathVariable("id") int id){ 
		try { 
			Produto produto = produtoComponent.buscarPorId(id);			
			return new ResponseEntity<Object>(produto, null, HttpStatus.OK);
		} catch (ConsistirException e) {
			return new ResponseEntity<Object>(e.getListaErros(), null, HttpStatus.INTERNAL_SERVER_ERROR);
		} 
	}
	
	@RequestMapping(value="/produtos/{id}", method=RequestMethod.DELETE, produces=MediaType.APPLICATION_JSON_VALUE)	
	public @ResponseBody ResponseEntity<Object> remover(@PathVariable("id") int id){ 
		try { 
			produtoComponent.remover(id);	
			return new ResponseEntity<Object>(null, null, HttpStatus.OK);
		} catch (ConsistirException e) {
			return new ResponseEntity<Object>(e.getListaErros(), null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	

	@RequestMapping(value="/produtos", method={RequestMethod.POST, RequestMethod.PUT}, produces=MediaType.APPLICATION_JSON_VALUE)	
	public @ResponseBody ResponseEntity<Object> salvar(@RequestBody Produto produto){ 
		try { 
			produtoComponent.persistir(produto);
			return new ResponseEntity<Object>(null, null, HttpStatus.CREATED);
		} catch (ConsistirException e) {
			return new ResponseEntity<Object>(e, null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
