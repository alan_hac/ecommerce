package br.com.fiap.ecommerce.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.fiap.ecommerce.component.PessoaComponent;
import br.com.fiap.ecommerce.dto.PessoaDTO;
import br.com.fiap.ecommerce.entity.Endereco;
import br.com.fiap.ecommerce.entity.Pessoa;
import br.com.fiap.uteis.ConsistirException;
import br.com.fiap.uteis.TokenUteis;

/**
 * Controller Criado para as funcionalidades referente ao CRUD Pessoas.
 * 
 * <b>Requisições</b>
 * Deve conter o Padrão RestFULL conforme descrito em {@link http://www.restapitutorial.com/lessons/restfulresourcenaming.html}
 * 
 *  <b>URL</b>            <b>HTTP</b>      <b>Funcionalidade</b>
 * \api\pessoas                  GET            listar todas as pessoas (Admin Apenas)
 * \pessoas\:id              GET            Buscar uma unica pessoa
 * \pessoas                  POST           Salva uma pessoa
 * \pessoas                  PUT            Atualiza uma pessoa
 * 
 * <b>Status de Retorno</b>
 * 
 * HttpStatus.OK = 200 - Requisição bem sucedida contendo o json compativel com a requisição feita
 * HttpStatus.CREATED = 201 - Requisição bem sucedida referente a uma ação POST de criação, contendo 
 * HttpStatus.INTERNAL_SERVER_ERROR = 500 - Retorno contendo a lista de mensagens com o problema para poder ser tratado no front
 * 
 * @author Paulo Porto
 *
 */
@RestController
public class ClienteController extends SuperController  {
	static Logger log = Logger.getLogger(ClienteController.class.getName());

	@Autowired
	private PessoaComponent pessoaComponent;
	
	@RequestMapping(value="/api/cliente/editar", method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)	
	public @ResponseBody ResponseEntity<Object> consultarPorId(HttpServletRequest request){ 
		try {
			//Pessoa pessoa = new Pessoa();
			Integer id = TokenUteis.getIdUsuario(TokenUteis.getToken(request));
			Pessoa pessoa = pessoaComponent.carregarCliente(id);
			return new ResponseEntity<Object>(pessoa, null, HttpStatus.OK);
	/*	} catch (ConsistirException e) {
			return geraRetornoErro(e);
		}*/
		} catch (Exception e) {
			return new ResponseEntity<Object>(e.getMessage(), null, HttpStatus.OK);
		} 
	}
 
	@RequestMapping(value="/cliente/cadastrar", method={RequestMethod.POST, RequestMethod.PUT}, consumes=MediaType.APPLICATION_JSON_VALUE, produces=MediaType.APPLICATION_JSON_VALUE)	
	public @ResponseBody ResponseEntity<Object> salvar(@RequestBody PessoaDTO pessoa){ 
		try {  
			// TODO ver se email já cadastrado 
			
			pessoaComponent.cadastrarCliente(pessoa.toPessoa());
			log.info("incluido cliente " + pessoa.getIdPessoa() + " Nome: " +  pessoa.getNome());
			return new ResponseEntity<Object>(pessoa.getIdPessoa(), null, HttpStatus.CREATED);
		} catch (ConsistirException e) {
			return geraRetornoErro(e);
		} 
	}
	
	@RequestMapping(value="/api/cliente/enderecos", method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody ResponseEntity<Object> buscarEnderecos(HttpServletRequest request){
		try {
			List<Endereco> enderecos = pessoaComponent.buscarEnderecos(TokenUteis.getIdUsuario(TokenUteis.getToken(request)));
			return new ResponseEntity<Object>(enderecos, null, HttpStatus.OK);
		} catch (ConsistirException e) {
			return geraRetornoErro(e);
		} 
	}
}
