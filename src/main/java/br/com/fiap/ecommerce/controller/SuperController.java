package br.com.fiap.ecommerce.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import br.com.fiap.ecommerce.component.PessoaComponent;
import br.com.fiap.ecommerce.entity.Pessoa;
import br.com.fiap.uteis.ConsistirException;
import br.com.fiap.uteis.TokenUteis;

public class SuperController {
	
	protected Pessoa usuario;
	
	@Autowired
	private PessoaComponent pessoaComponent;

	protected ResponseEntity<Object> geraRetornoErro(ConsistirException e) {
		return new ResponseEntity<Object>(e, null, HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
	protected Pessoa getUsuarioLogado(HttpServletRequest request) throws ConsistirException{
		Integer idUsuario = TokenUteis.getIdUsuario(TokenUteis.getToken(request));
		
		return  pessoaComponent.buscarUsuario(idUsuario);	
		
	}
	
	
}
