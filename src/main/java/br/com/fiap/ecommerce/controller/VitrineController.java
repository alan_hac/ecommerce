package br.com.fiap.ecommerce.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.fiap.ecommerce.component.ProdutoComponent;
import br.com.fiap.ecommerce.dto.FiltroProdutosDTO;
import br.com.fiap.ecommerce.dto.ProdutoDTO;
import br.com.fiap.ecommerce.entity.Produto;
import br.com.fiap.ecommerce.enums.CategoriaEnum;
import br.com.fiap.uteis.ConsistirException;

/**
 * Controller que irá conter os métodos referentes a Vitrine de produtos. onde irá exibir para o cliente não logado e/ou logado os produtos disponiveis
 * 
 * @author Paulo Porto
 *
 */
@RestController
@RequestMapping(value="/vitrine")
public class VitrineController extends SuperController  {
	static Logger log = Logger.getLogger(VitrineController.class.getName());

	@Autowired
	private ProdutoComponent produtoComponent; 
	
	@RequestMapping(value="/produtos", method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)	
	public @ResponseBody ResponseEntity<Object> listar(){ 
		try { 
			List<Produto> produtos = produtoComponent.buscarProdutos();
			return  new ResponseEntity<Object>(produtos, null, HttpStatus.OK);
		} catch (ConsistirException e) {
			return geraRetornoErro(e);
		}
	}
	
	@RequestMapping(value="/produtos/{codigoProduto}", method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)	
	public @ResponseBody ResponseEntity<Object> buscarPorId(@PathVariable("codigoProduto") int codigoProduto){ 
		try { 
			Produto produto = produtoComponent.buscarPorId(codigoProduto);
			return  new ResponseEntity<Object>(produto, null, HttpStatus.OK);
		} catch (ConsistirException e) {
			return geraRetornoErro(e);
		}
	}

	@RequestMapping(value="/produtos/categorias", method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)	
	public @ResponseBody ResponseEntity<Object> categorias(){ 
		try { 
			return new ResponseEntity<Object>(CategoriaEnum.values(), null, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<Object>(e, null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@RequestMapping(value="/produtos/filtrar", method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)	
	public @ResponseBody ResponseEntity<Object> consultar(@RequestBody FiltroProdutosDTO filtro){ 
		try { 
			List<Produto> produtos = produtoComponent.buscarPorFiltros(filtro);			
			return new ResponseEntity<Object>(produtos, null, HttpStatus.OK);
		} catch (ConsistirException e) {
			return geraRetornoErro(e);
		} 
	}
	
	@RequestMapping(value="/produtos/validarEstoque", method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)	
	public @ResponseBody ResponseEntity<Object> consultar(@RequestBody List<ProdutoDTO> produtos){ 
		try { 
			produtoComponent.validarCarrinho(produtos);
			return new ResponseEntity<Object>("OK", null, HttpStatus.OK);
		} catch (ConsistirException e) {
			return geraRetornoErro(e);
		} 
	}

}
