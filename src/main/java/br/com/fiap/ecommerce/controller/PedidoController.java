package br.com.fiap.ecommerce.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.fiap.ecommerce.component.PedidoComponent;
import br.com.fiap.ecommerce.dto.FiltrosPedidoDTO;
import br.com.fiap.ecommerce.dto.PedidoDTO;
import br.com.fiap.ecommerce.entity.Pedido;
import br.com.fiap.uteis.ConsistirException;
/**
 * TODO JAVADOC
 */

@RestController
public class PedidoController extends SuperController  {
	static Logger log = Logger.getLogger(PedidoController.class.getName());

	@Autowired
	private PedidoComponent pedidoComponent; 

	@RequestMapping(value = "/api/pedidos", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody ResponseEntity<Object> listar(HttpServletRequest request, @RequestBody FiltrosPedidoDTO filtros) {
		try {
			List<Pedido> pedidos = pedidoComponent.consultar(filtros, this.getUsuarioLogado(request)); 
			return new ResponseEntity<Object>(pedidos, null, HttpStatus.OK);
		} catch (ConsistirException e) {
			return geraRetornoErro(e);
		} 
	}
	
	/**
	 * Método responsável por obter os pedidos de um cliente
	 * 
	 * @param id
	 * @return
	 */
	@RequestMapping(value="/api/pedidos/{id}", method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)	
	public @ResponseBody ResponseEntity<Object> consultarPorId(HttpServletRequest request, @PathVariable("id") int id){ 
		try { 
			Pedido pedido = pedidoComponent.buscarPedido(id, this.getUsuarioLogado(request));
			return new ResponseEntity<Object>(pedido, null, HttpStatus.OK);
		} catch (ConsistirException e) {
			return geraRetornoErro(e);
		}
	}

	/**
	 * Método responsável pela primeira passagem.
	 * Após passar pelo carrinho ele irá realizar a solicitação do pedido. Neste momento deverá ser feito 
	 * a validação de estoque e etc...
	 * 
	 * @param pedido
	 * @return
	 */
	@RequestMapping(value="/api/pedido/confirmar", method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)	
	public @ResponseBody  ResponseEntity<Object> solicitarPedido(HttpServletRequest request, @RequestBody PedidoDTO pedidoDTO){
		try { 
			Pedido pedido = pedidoComponent.confirmarPedido(pedidoDTO, this.getUsuarioLogado(request));
			return new ResponseEntity<Object>(pedido, null, HttpStatus.CREATED);
		} catch (ConsistirException e) {
			return geraRetornoErro(e);
		}
	}

	/**
	 * Método responsável pela primeira passagem.
	 * Após passar pelo carrinho ele irá realizar a solicitação do pedido. Neste momento deverá ser feito 
	 * a validação de estoque e etc...
	 * 
	 * @param pedido
	 * @return
	 */
	@RequestMapping(value="/api/pedido/cancelar", method=RequestMethod.POST, produces=MediaType.APPLICATION_JSON_VALUE)	
	public @ResponseBody  ResponseEntity<Object> cancelarPedido(HttpServletRequest request, @RequestBody Integer idPedido){
		try { 
			pedidoComponent.cancelarPedido(idPedido, this.getUsuarioLogado(request));
			return new ResponseEntity<Object>(idPedido, null, HttpStatus.CREATED);
		} catch (ConsistirException e) {
			return geraRetornoErro(e);
		}
	}
}
