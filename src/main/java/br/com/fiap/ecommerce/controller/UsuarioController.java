package br.com.fiap.ecommerce.controller;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.fiap.ecommerce.component.UsuarioComponent;
import br.com.fiap.ecommerce.entity.Usuario;
import br.com.fiap.uteis.ConsistirException;
import br.com.fiap.uteis.TokenUteis;

/**
 * Controller Responsável por realizar as funcionalidades de acesso.
 * 
 * @author Alan
 */
@RestController
public class UsuarioController extends SuperController {
	static Logger log = Logger.getLogger(UsuarioController.class.getName());

	@Autowired
	private UsuarioComponent usuarioComponent;

	/**
	 * Método que é responsável por validar se o usuário está cadastrado corretamente no banco e encapsular seus dados em um token;
	 * 
	 * @param usuario
	 * @return
	 * @throws ServletException
	 * @throws ConsistirException
	 */
	@RequestMapping(value = "/usuario/login", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody ResponseEntity<Object> login(@RequestBody Usuario usuario) throws ServletException, ConsistirException {
		try {
			String token = usuarioComponent.validaLogin(usuario.getLogin(), usuario.getSenha());
			return new ResponseEntity<>(new LoginResponse(token) , HttpStatus.ACCEPTED);
		} catch (ConsistirException e) {
			e.setStatus(HttpStatus.UNAUTHORIZED);
			return new ResponseEntity<>(e, HttpStatus.UNAUTHORIZED);
		}
	}
	
	/**
	 * Método responsável por verificar quais permissões de acesso o usuário tem
	 * @param role
	 * @param request
	 * @return
	 * @throws ServletException
	 */
	@RequestMapping(value = "/api/usuario/perfil/{role}", method = RequestMethod.GET)
	public Boolean temPerfil(@PathVariable final String role, final HttpServletRequest request) throws ServletException {
		return TokenUteis.possuiPermissao(TokenUteis.getToken(request), role);
	}
	
	@SuppressWarnings("unused")
	private static class LoginResponse {
		public String token;

		public LoginResponse(final String token) {
			this.token = token;
		}
	}
}
