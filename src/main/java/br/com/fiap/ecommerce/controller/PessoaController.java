package br.com.fiap.ecommerce.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.fiap.ecommerce.component.PessoaComponent;
import br.com.fiap.ecommerce.component.UsuarioComponent;
import br.com.fiap.ecommerce.dto.FiltroPessoaDTO;
import br.com.fiap.ecommerce.entity.Pessoa;
import br.com.fiap.uteis.ConsistirException;

/**
 * Controller Criado para as funcionalidades referente ao CRUD Pessoas.
 * 
 * <b>Requisições</b>
 * Deve conter o Padrão RestFULL conforme descrito em {@link http://www.restapitutorial.com/lessons/restfulresourcenaming.html}
 * 
 *  <b>URL</b>            <b>HTTP</b>      <b>Funcionalidade</b>
 * \api\pessoas                  GET            listar todas as pessoas (Admin Apenas)
 * \pessoas\:id              GET            Buscar uma unica pessoa
 * \pessoas                  POST           Salva uma pessoa
 * \pessoas                  PUT            Atualiza uma pessoa
 * 
 * <b>Status de Retorno</b>
 * 
 * HttpStatus.OK = 200 - Requisição bem sucedida contendo o json compativel com a requisição feita
 * HttpStatus.CREATED = 201 - Requisição bem sucedida referente a uma ação POST de criação, contendo 
 * HttpStatus.INTERNAL_SERVER_ERROR = 500 - Retorno contendo a lista de mensagens com o problema para poder ser tratado no front
 * 
 * @author Paulo Porto
 *
 */
@RestController
public class PessoaController extends SuperController  {
	static Logger log = Logger.getLogger(PessoaController.class.getName());

	@Autowired
	private PessoaComponent pessoaComponent; 
	@Autowired
	private UsuarioComponent usuarioComponent; 
	
	@RequestMapping(value = "/api/pessoas/", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody ResponseEntity<Object> listar(@RequestBody FiltroPessoaDTO filtros) {
		try {
			List<Pessoa> pessoas = pessoaComponent.buscarPorFiltros(filtros);
			return new ResponseEntity<Object>(pessoas, null, HttpStatus.OK);
		} catch (ConsistirException e) {
			return geraRetornoErro(e);
		}
	}
 
	@RequestMapping(value="/api/pessoas/{id}", method=RequestMethod.GET, produces=MediaType.APPLICATION_JSON_VALUE)	
	public @ResponseBody ResponseEntity<Object> consultarPorId(@PathVariable("id") int id){ 
		try { 
			Pessoa pessoa = pessoaComponent.buscarPorId(id);			
			return new ResponseEntity<Object>(pessoa, null, HttpStatus.OK);
		} catch (ConsistirException e) {
			return geraRetornoErro(e);
		}
	}

	@RequestMapping(value="/api/pessoas/{id}", method=RequestMethod.DELETE, produces=MediaType.APPLICATION_JSON_VALUE)	
	public @ResponseBody ResponseEntity<Object> removerPorId(@PathVariable("id") int id){ 
		try { 
			pessoaComponent.remover(id);
			return new ResponseEntity<Object>(null, null, HttpStatus.OK);
		} catch (ConsistirException e) {
			return geraRetornoErro(e);
		}
	}

	@RequestMapping(value="/api/pessoas", method={RequestMethod.POST, RequestMethod.PUT}, produces=MediaType.APPLICATION_JSON_VALUE)	
	public @ResponseBody ResponseEntity<Object> salvar(@RequestBody Pessoa pessoa){ 
		try {  
			pessoaComponent.persistir(pessoa);
			return new ResponseEntity<Object>(null, null, HttpStatus.CREATED);
		} catch (ConsistirException e) {
			return geraRetornoErro(e);
		}
	}
}
