package br.com.fiap.ecommerce.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import br.com.fiap.ecommerce.entity.Endereco;

@Transactional(rollbackFor = Exception.class)
public interface EnderecoRepository extends CrudRepository<Endereco, Integer>{

}
