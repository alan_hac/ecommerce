package br.com.fiap.ecommerce.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import br.com.fiap.ecommerce.entity.Pedido;
import br.com.fiap.ecommerce.entity.PedidoProduto;

@Transactional(rollbackFor = Exception.class)
public interface PedidoRepository extends CrudRepository<Pedido, Integer>{

	@Query("select p from Pedido p where p.idPedido = ?1 and p.pessoa.idPessoa = ?2")
	Pedido buscarPedidoPorPessoa(Integer idPedido, int idPessoa);
	

	@Query("select p from Pedido p where p.pessoa.idPessoa = ?1")
	Pedido buscarPedidosPorPessoa(int idPessoa);

}
