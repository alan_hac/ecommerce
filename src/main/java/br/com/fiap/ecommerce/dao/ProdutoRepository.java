package br.com.fiap.ecommerce.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import br.com.fiap.ecommerce.entity.Produto;

@Transactional(rollbackFor = Exception.class)
public interface ProdutoRepository extends CrudRepository<Produto, Integer>{

}
