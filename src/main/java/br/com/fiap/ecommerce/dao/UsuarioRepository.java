package br.com.fiap.ecommerce.dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import br.com.fiap.ecommerce.entity.Usuario;

@Transactional(rollbackFor = Exception.class)
public interface UsuarioRepository extends CrudRepository<Usuario, Integer>{

	@Query("Select u from Usuario u where u.login = ?1 and u.senha = ?2")
	public Usuario buscarUsuarioPorLoginESenha(String login, String senha);
}
