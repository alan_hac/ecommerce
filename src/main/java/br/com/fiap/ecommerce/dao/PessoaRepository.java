package br.com.fiap.ecommerce.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import br.com.fiap.ecommerce.entity.Endereco;
import br.com.fiap.ecommerce.entity.Pessoa;

@Transactional(rollbackFor = Exception.class)
public interface PessoaRepository extends CrudRepository<Pessoa, Integer>{

	  
 
	@Query("Select e from Endereco e where e.pessoa.usuario.id = ?1") 
	public List<Endereco> buscarEnderecosPorPessoa(Integer idPessoa);
	
	@Query("Select p from Pessoa p where p.usuario.id = ?1")
	public Pessoa buscarPessoaPorUsuario(Integer idUsuario);

}
