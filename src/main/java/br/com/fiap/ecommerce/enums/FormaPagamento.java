package br.com.fiap.ecommerce.enums;

import com.google.common.collect.Lists;

public enum FormaPagamento {
		CARTAO("C", "CARTAO"),
		BOLETO("B", "BOLETO");
		
	
		private FormaPagamento(String codigo, String descricao) {
			this.codigo = codigo;
			this.descricao = descricao;
		}
		
		private String codigo;
		
		private String descricao;
	
		public boolean isBoleto(){
			return false;
		}
		public boolean isCartao(){
			return false;
		}

		public String getCodigo() {
			return codigo;
		}

		public String getDescricao() {
			return descricao;
		}

		public static FormaPagamento findByDescricao(String formaPagamento) {
			return Lists.newArrayList(FormaPagamento.values())
			.stream()
			.filter(f -> f.descricao.equals(formaPagamento))
			.findFirst().get();
		}
	}