package br.com.fiap.ecommerce.enums;

public enum StatusPedidoEnum {
	AGUARDANDO("Aguardando Pagamento"),
	REPROVADO("Pagamento Recusado"),
	APROVADO("Pagamento Aprovado"),
	CANCELADO("Pedido Cancelado"),
	SOLICITADO("Pedido Solicitado");
	
	private String descricao;
	
	private StatusPedidoEnum(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}

}
