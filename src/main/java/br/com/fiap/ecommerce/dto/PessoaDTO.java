package br.com.fiap.ecommerce.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import br.com.fiap.ecommerce.entity.Endereco;
import br.com.fiap.ecommerce.entity.Pessoa;

public class PessoaDTO  implements Serializable {
 
	private static final long serialVersionUID = 1L;

	private Integer idPessoa;
 
	private UsuarioDTO usuario;
	
	private String documento;

	private String nome;
 
	private List<EnderecoDTO> enderecos;
   
	
	
	public Pessoa toPessoa(){
		Pessoa pessoa = new Pessoa();
		
		pessoa.setIdPessoa(this.idPessoa);
		pessoa.setUsuario(this.usuario.toUsuario());
		pessoa.setNome(this.nome);
		pessoa.setDocumento(this.documento);
		pessoa.setEnderecos(toEnderecos());
		return pessoa;
	}
	
	public List<Endereco> toEnderecos(){
		List<Endereco> listaEnderecos = new ArrayList<Endereco>();
		
		for (EnderecoDTO enderecoDTO : this.enderecos){
			listaEnderecos.add(enderecoDTO.toEndereco()); 
		}
		return listaEnderecos; 
	}
	
	public PessoaDTO(){
		super();
	}

	public Integer getIdPessoa() {
		return idPessoa;
	}

	public void setIdPessoa(Integer idPessoa) {
		this.idPessoa = idPessoa;
	}

	public UsuarioDTO getUsuario() {
		return usuario;
	}

	public void setUsuario(UsuarioDTO usuario) {
		this.usuario = usuario;
	}

	public String getDocumento() {
		return documento;
	}

	public void setDocumento(String documento) {
		this.documento = documento;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public List<EnderecoDTO> getEnderecos() {
		return enderecos;
	}

	public void setEnderecos(List<EnderecoDTO> enderecos) {
		this.enderecos = enderecos;
	}
 
	
}
