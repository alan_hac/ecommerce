package br.com.fiap.ecommerce.dto;

public class RetornoDTO {
	public String status; 
	public Object retorno;
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Object getRetorno() {
		return retorno;
	}
	public void setRetorno(Object retorno) {
		this.retorno = retorno;
	}
	
	
}
