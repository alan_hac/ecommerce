package br.com.fiap.ecommerce.dto;

import java.math.BigDecimal;

import br.com.fiap.ecommerce.enums.CategoriaEnum;

public class FiltroProdutosDTO { 
	private String nome;
	private String fonecedor;
	private String ncm;
	private BigDecimal precoInicio;
	private BigDecimal precoFim;
	private Integer estoqueInicio;
	private Integer estoqueFim;
    private CategoriaEnum categoria;
	private Boolean destaque;
	private BigDecimal percentualDesconto;
	private Integer offSet;
	private Integer tamanhoLista;
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getFonecedor() {
		return fonecedor;
	}
	public void setFonecedor(String fonecedor) {
		this.fonecedor = fonecedor;
	}
	public String getNcm() {
		return ncm;
	}
	public void setNcm(String ncm) {
		this.ncm = ncm;
	}  
	public Integer getEstoqueInicio() {
		return estoqueInicio;
	}
	public void setEstoqueInicio(Integer estoqueInicio) {
		this.estoqueInicio = estoqueInicio;
	}
	public Integer getEstoqueFim() {
		return estoqueFim;
	}
	public void setEstoqueFim(Integer estoqueFim) {
		this.estoqueFim = estoqueFim;
	}
	public CategoriaEnum getCategoria() {
		return categoria;
	}
	public void setCategoria(CategoriaEnum categoria) {
		this.categoria = categoria;
	}
	public Boolean getDestaque() {
		return destaque;
	}
	public void setDestaque(Boolean destaque) {
		this.destaque = destaque;
	}
	public BigDecimal getPercentualDesconto() {
		return percentualDesconto;
	}
	public void setPercentualDesconto(BigDecimal percentualDesconto) {
		this.percentualDesconto = percentualDesconto;
	}
	public BigDecimal getPrecoInicio() {
		return precoInicio;
	}
	public void setPrecoInicio(BigDecimal precoInicio) {
		this.precoInicio = precoInicio;
	}
	public BigDecimal getPrecoFim() {
		return precoFim;
	}
	public void setPrecoFim(BigDecimal precoFim) {
		this.precoFim = precoFim;
	}
	public Integer getOffSet() {
		return offSet;
	}
	public void setOffSet(Integer offSet) {
		this.offSet = offSet;
	}
	public Integer getTamanhoLista() {
		return tamanhoLista;
	}
	public void setTamanhoLista(Integer tamanhoLista) {
		this.tamanhoLista = tamanhoLista;
	}
	 
}
