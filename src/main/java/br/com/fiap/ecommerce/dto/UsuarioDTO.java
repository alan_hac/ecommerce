package br.com.fiap.ecommerce.dto;

import br.com.fiap.ecommerce.entity.Usuario;

public class UsuarioDTO {
 
	private Integer id;
 
	private String login;
	
	private String senha;
	
	
	public Usuario toUsuario(){
		Usuario usuario = new Usuario();
		
		usuario.setId(this.id);
		usuario.setLogin(this.login);
		usuario.setSenha(this.senha);
		
		return usuario;
	}


	public UsuarioDTO(){
		super();
	}
	public Integer getId() {
		return id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public String getLogin() {
		return login;
	}


	public void setLogin(String login) {
		this.login = login;
	}


	public String getSenha() {
		return senha;
	}


	public void setSenha(String senha) {
		this.senha = senha;
	} 
}
