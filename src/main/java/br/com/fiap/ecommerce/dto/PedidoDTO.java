package br.com.fiap.ecommerce.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import br.com.fiap.ecommerce.entity.Endereco;

@JsonIgnoreProperties(ignoreUnknown= true)
public class PedidoDTO implements Serializable {

	private static final long serialVersionUID = 1L;

	private Endereco enderecoEntrega;
	
	private List<ProdutoDTO> produtos = new ArrayList<>();
	
	private Double valorTotal;
	
	public PedidoDTO() {
		super();
	}

	public PedidoDTO(Endereco enderecoEntrega, List<ProdutoDTO> produtos, Double valorTotal) {
		super();
		this.enderecoEntrega = enderecoEntrega;
		this.produtos = produtos;
		this.valorTotal = valorTotal;
	}

	public Endereco getEnderecoEntrega() {
		return enderecoEntrega;
	}

	public void setEnderecoEntrega(Endereco enderecoEntrega) {
		this.enderecoEntrega = enderecoEntrega;
	}

	public List<ProdutoDTO> getProdutos() {
		return produtos;
	}

	public void setProdutos(List<ProdutoDTO> produtos) {
		this.produtos = produtos;
	}

	public Double getValorTotal() {
		return valorTotal;
	}

	public void setValorTotal(Double valorTotal) {
		this.valorTotal = valorTotal;
	}
	
}
