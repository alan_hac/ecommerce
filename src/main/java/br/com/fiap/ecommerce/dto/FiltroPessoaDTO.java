package br.com.fiap.ecommerce.dto;

public class FiltroPessoaDTO { 
	private String nome;
	private String documento;
	private String email;
	private Integer offSet;
	private Integer tamanhoLista;
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getDocumento() {
		return documento;
	}
	public void setDocumento(String documento) {
		this.documento = documento;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Integer getOffSet() {
		return offSet;
	}
	public void setOffSet(Integer offSet) {
		this.offSet = offSet;
	}
	public Integer getTamanhoLista() {
		return tamanhoLista;
	}
	public void setTamanhoLista(Integer tamanhoLista) {
		this.tamanhoLista = tamanhoLista;
	}
	
	
}
