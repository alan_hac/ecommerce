package br.com.fiap.ecommerce.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import br.com.fiap.ecommerce.enums.CategoriaEnum;


/**
 * The persistent class for the produto database table.
 * 
 */
@Entity
@NamedQuery(name="Produto.findAll", query="SELECT p FROM Produto p")
@JsonIgnoreProperties(ignoreUnknown=true)
public class Produto implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id_produto")
	private Integer idProduto;
 
	private String nome;
 
	private String descricao;

	@Column(name="fonecedor") 
	private String fornecedor;

	@Column(name="ncm") 
	private String ncm;

	@Column(name="peso") 
	private Double peso;

	@Column(name="preco") 
	private Double preco;

	@Column(name="estoque") 
	private Integer estoque;

	@Enumerated(EnumType.STRING) 
    private CategoriaEnum categoria;

	@Column(name="destaque") 
	private Boolean destaque;
	
	@Column(name="percentual_desconto") 
	private BigDecimal percentualDesconto;
 
	@Column(name="imagem") 
	private String foto;
	  
	@OneToMany(mappedBy="produto")
	private List<ProdutoImagens> imagens;
	


	public Produto() {
	}

	public Integer getIdProduto() {
		return idProduto;
	}

	public void setIdProduto(Integer idProduto) {
		this.idProduto = idProduto;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getFornecedor() {
		return fornecedor;
	}

	public void setFornecedor(String fonecedor) {
		this.fornecedor = fonecedor;
	}

	public String getNcm() {
		return ncm;
	}

	public void setNcm(String ncm) {
		this.ncm = ncm;
	}

	public Double getPeso() {
		return peso;
	}

	public void setPeso(Double peso) {
		this.peso = peso;
	}

	public Double getPreco() {
		return preco;
	}

	public void setPreco(Double preco) {
		this.preco = preco;
	}

	public CategoriaEnum getCategoria() {
		return categoria;
	}

	public void setCategoria(CategoriaEnum categoria) {
		this.categoria = categoria;
	}

	public Boolean getDestaque() {
		return destaque;
	}

	public void setDestaque(Boolean destaque) {
		this.destaque = destaque;
	}

	public BigDecimal getPercentualDesconto() {
		return percentualDesconto;
	}

	public void setPercentualDesconto(BigDecimal percentualDesconto) {
		this.percentualDesconto = percentualDesconto;
	}

	public String getFoto() {
		return foto;
	}

	public void setFoto(String foto) {
		this.foto = foto;
	}

	public List<ProdutoImagens> getImagens() {
		return imagens;
	}

	public void setImagens(List<ProdutoImagens> imagens) {
		this.imagens = imagens;
	} 

	public Integer getEstoque() {
		return estoque;
	}

	public void setEstoque(Integer estoque) {
		this.estoque = estoque;
	}
}
