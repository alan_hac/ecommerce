package br.com.fiap.ecommerce.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import br.com.fiap.ecommerce.enums.StatusPedidoEnum;


/**
 * The persistent class for the pedido database table.
 * 
 */
@Entity
@NamedQuery(name="Pedido.findAll", query="SELECT p FROM Pedido p")
@JsonIgnoreProperties(ignoreUnknown=true)
public class Pedido implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY) 
	@Column(name="id_pedido") 
	private Integer idPedido;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="data_pedido")
	private Date dataPedido;
	
	@Enumerated(EnumType.STRING)
	private StatusPedidoEnum situacao;

	@Column(name="valor_total")
	private Double valorTotal;

	//bi-directional many-to-one association to Cliente
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="id_pessoa")
	@JsonBackReference
	private Pessoa pessoa;

	//bi-directional many-to-one association to PedidoProduto
	@OneToMany(mappedBy="pedido", cascade=CascadeType.ALL ,fetch=FetchType.EAGER)
	@JsonIdentityInfo(generator=ObjectIdGenerators.IntSequenceGenerator.class, property="@id")
	private List<PedidoProduto> pedidoProdutos = new ArrayList<>();

	@ManyToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="id_endereco")
	private Endereco enderecoEntrega;
	 
	@OneToOne(cascade=CascadeType.ALL)
    @JoinColumn(name="id_boleto") 
	private Boleto boleto;
 
 

	public Pedido() {
	}

	public Integer getIdPedido() {
		return this.idPedido;
	}

	public void setIdPedido(Integer idPedido) {
		this.idPedido = idPedido;
	}

	public Date getDataPedido() {
		return this.dataPedido;
	}

	public void setDataPedido(Date dataPedido) {
		this.dataPedido = dataPedido;
	}

	public StatusPedidoEnum getSituacao() {
		return this.situacao;
	}

	public void setSituacao(StatusPedidoEnum situacao) {
		this.situacao = situacao;
	}

	public Double getValorTotal() {
		return this.valorTotal;
	}

	public void setValorTotal(Double valorTotal) {
		this.valorTotal = valorTotal;
	} 

	public Pessoa getPessoa() {
		return pessoa;
	}

	public void setPessoa(Pessoa pessoa) {
		this.pessoa = pessoa;
	}

	public List<PedidoProduto> getPedidoProdutos() {
		return this.pedidoProdutos;
	}

	public void setPedidoProdutos(List<PedidoProduto> pedidoProdutos) {
		this.pedidoProdutos = pedidoProdutos;
	}
	
	public Endereco getEnderecoEntrega() {
		return enderecoEntrega;
	}

	public void setEnderecoEntrega(Endereco enderecoEntrega) {
		this.enderecoEntrega = enderecoEntrega;
	}
 
	public Boleto getBoleto() {
		return boleto;
	}

	public void setBoleto(Boleto boleto) {
		this.boleto = boleto;
	}

	public PedidoProduto addPedidoProduto(PedidoProduto pedidoProduto) {
		getPedidoProdutos().add(pedidoProduto);
		pedidoProduto.setPedido(this);

		return pedidoProduto;
	}

	public PedidoProduto removePedidoProduto(PedidoProduto pedidoProduto) {
		getPedidoProdutos().remove(pedidoProduto);
		pedidoProduto.setPedido(null);

		return pedidoProduto;
	}
 

}