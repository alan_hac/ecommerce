package br.com.fiap.ecommerce.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name="boleto")
@NamedQuery(name="Boleto.findAll", query="SELECT b FROM Boleto b")
@JsonIgnoreProperties(ignoreUnknown=true)
public class Boleto implements Serializable {
 
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY) 
	@Column(name="id") 
	private Integer id;
 
	private String numero_boleto;
	
	@OneToOne(cascade=CascadeType.ALL, mappedBy="boleto") 
    @JoinColumn(name="id_pedido") 
	private Pedido pedido;

	@Column(name="id_pedido")
	private Integer idpedido;

	@Column(name="valorboleto")
	private Double valorboleto;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="datavencimento")
	private Date datavencimento;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

 
	public String getNumero_boleto() {
		return numero_boleto;
	}

	public void setNumero_boleto(String numero_boleto) {
		this.numero_boleto = numero_boleto;
	}

	public Pedido getPedido() {
		return pedido;
	}

	public void setPedido(Pedido pedido) {
		this.pedido = pedido;
	}
 

	public Double getValorboleto() {
		return valorboleto;
	}

	public void setValorboleto(Double valorboleto) {
		this.valorboleto = valorboleto;
	}

	public Date getDatavencimento() {
		return datavencimento;
	}

	public void setDatavencimento(Date datavencimento) {
		this.datavencimento = datavencimento;
	}

	public Integer getIdpedido() {
		return idpedido;
	}

	public void setIdpedido(Integer idpedido) {
		this.idpedido = idpedido;
	}
 
 

	 

}
