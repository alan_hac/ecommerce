package br.com.fiap.ecommerce.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


/**
 * The persistent class for the pedido_produto database table.
 * 
 */
@Entity
@Table(name="pedido_produto")
@NamedQuery(name="PedidoProduto.findAll", query="SELECT p FROM PedidoProduto p")
@JsonIgnoreProperties(ignoreUnknown=true)
public class PedidoProduto implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id")
	private Integer id;
	
	private Integer quantidade;

	private Double valor;
 
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="id_pedido")
	@JsonBackReference
	private Pedido pedido;
 
	@ManyToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="id_produto")
	@JsonBackReference
	private Produto produto;

	public PedidoProduto() {
	}
 
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getQuantidade() {
		return this.quantidade;
	}

	public void setQuantidade(Integer quantidade) {
		this.quantidade = quantidade;
	}

	public Double getValor() {
		return this.valor;
	}

	public void setValor(Double valor) {
		this.valor = valor;
	}

	public Pedido getPedido() {
		return this.pedido;
	}

	public void setPedido(Pedido pedido) {
		this.pedido = pedido;
	}

	public Produto getProduto() {
		return this.produto;
	}

	public void setProduto(Produto produto) {
		this.produto = produto;
	}

}