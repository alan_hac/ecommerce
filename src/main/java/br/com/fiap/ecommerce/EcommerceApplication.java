package br.com.fiap.ecommerce;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.embedded.FilterRegistrationBean;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import br.com.fiap.ecommerce.filter.LoginFilter;

@SpringBootApplication
@Configuration
@ComponentScan(basePackages="br.com.fiap.ecommerce")
@EnableScheduling
@EnableCaching
public class EcommerceApplication {

	public static void main(String[] args) {
		ApplicationContext ctx = SpringApplication.run(EcommerceApplication.class, args);
	}
	
	@Bean
    public FilterRegistrationBean loginFilter() {
        final FilterRegistrationBean registrationBean = new FilterRegistrationBean();
        registrationBean.setFilter(new LoginFilter());
        registrationBean.addUrlPatterns("/api/*");

        return registrationBean;
    }
    
    @Bean
    public WebMvcConfigurer corsConfigurer() {
    	return new WebMvcConfigurerAdapter() {
		    @Override
		    public void addCorsMappings(CorsRegistry registry) {
		    registry.addMapping("/api/**").allowedOrigins("http://localhost:9000").allowCredentials(true)
		    .exposedHeaders("Access-Control-Allow-Origin: *")
		    .allowedMethods("GET", "HEAD", "POST", "PUT", "PATCH", "DELETE", "OPTIONS", "TRACE")
		    .allowedHeaders("content-type", "X-Requested-With", "Authorization","AuthorizationPortador");
		    }
	    };
    }
}
