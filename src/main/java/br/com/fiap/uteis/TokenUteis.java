package br.com.fiap.uteis;

import java.time.LocalDateTime;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.xml.bind.DatatypeConverter;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

public class TokenUteis {

	private static String key = "secretKey"; //"Não sei se implemento uma API para essa senha";
	private static SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;
//	static Key signingKey = new SecretKeySpec(DatatypeConverter.parseBase64Binary(key),
//			signatureAlgorithm.getJcaName());

	public static String criaToken(int id, String permissao, String usuario) throws JwtException{
		LocalDateTime dataToken = LocalDateTime.now();
		JwtBuilder builder = Jwts.builder()
								.setId(String.valueOf(id))
								.setSubject(usuario)
								.claim("permissao", permissao)
								.setIssuedAt(DateUtils.asDate(dataToken))
								.setExpiration(DateUtils.asDate(dataToken.plusHours(1L)))
								.signWith(signatureAlgorithm, key);
		return builder.compact();
	}
	
	public static Claims getClaims(String token){
		try {
			return Jwts.parser()
					.setSigningKey(DatatypeConverter.parseBase64Binary(key))
					.parseClaimsJws(token)
					.getBody();
		} catch (Exception e) {
			
			//TODO Logar Exceptions
			return null;
		}
	}
	
	public static String getPermissao(String token) throws JwtException{
		Claims claims = getClaims(token);
		return (String) claims.get("permissao");
	}
	
	public static Boolean isValido(String token) throws JwtException{
		Claims claims = getClaims(token);
		return claims.getExpiration().after(new Date(System.currentTimeMillis()));
	}
	
	public static String getUsuario(String token) throws JwtException{
		Claims claims = getClaims(token);
		return claims.getSubject();
	} 
 

	public static Claims obtemAssinaturaToken(String token) {
		return Jwts.parser().setSigningKey(key)
                .parseClaimsJws(token).getBody();
	} 
	public static Integer getIdUsuario(String token) {
		Claims claims = getClaims(token);
		return Integer.parseInt(claims.getId());
 
	}
	
	public static String getToken(HttpServletRequest request) {
		return request.getHeader("authorization");
	}

	public static Boolean possuiPermissao(String token, String role) {
		Claims claims = getClaims(token);	
		return claims.get("permissao").equals(role);
	}
}
