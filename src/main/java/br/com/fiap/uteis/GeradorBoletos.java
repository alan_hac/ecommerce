package br.com.fiap.uteis;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import br.com.caelum.stella.boleto.Banco;
import br.com.caelum.stella.boleto.Beneficiario;
import br.com.caelum.stella.boleto.Boleto;
import br.com.caelum.stella.boleto.Datas;
import br.com.caelum.stella.boleto.Endereco;
import br.com.caelum.stella.boleto.Pagador;
import br.com.caelum.stella.boleto.bancos.BancoDoBrasil;
import br.com.caelum.stella.boleto.transformer.GeradorDeBoleto;
import br.com.fiap.ecommerce.entity.Pedido;

public class GeradorBoletos {
 
		public static ByteArrayInputStream gerarBoletoPedido(Pedido pedido){
			byte[] bPDF =  null;
			ByteArrayOutputStream outputStream = null;
			ByteArrayInputStream bis = null; 
			try {
				// Cria datas do Boleto
				Calendar dataDocumento = new GregorianCalendar();
				dataDocumento.setTime(new Date());
				Calendar dataProcessamento = dataDocumento;
				// adiciona 7 dias para data do vencimento
				Calendar dataVencimento = Uteis.adicionarDiasEmData(new Date(), 7);
				
				 Datas datas = Datas.novasDatas()
				            .comDocumento(dataDocumento.get(Calendar.DAY_OF_MONTH), dataDocumento.get(Calendar.MONTH) +1 , dataDocumento.get(Calendar.YEAR))
				            .comProcessamento(dataProcessamento.get(Calendar.DAY_OF_MONTH), dataProcessamento.get(Calendar.MONTH) +1 , dataProcessamento.get(Calendar.YEAR))
				            .comVencimento(dataVencimento.get(Calendar.DAY_OF_MONTH), dataVencimento.get(Calendar.MONTH) +1 , dataVencimento.get(Calendar.YEAR)); 

				Endereco enderecoBeneficiario = Endereco.novoEndereco()
				        .comLogradouro("Av Lins de Vasconcelos")  
				        .comBairro("Aclimacao")  
				        .comCep("01234-555")  
				        .comCidade("São Paulo")  
				        .comUf("SP");  
				

				//Quem emite o boleto
				Beneficiario beneficiario = Beneficiario.novoBeneficiario()  
				        .comNomeBeneficiario("FIAP 26SCJ")  
				        .comAgencia("1824").comDigitoAgencia("4")  
				        .comCodigoBeneficiario("76000")  
				        .comDigitoCodigoBeneficiario("5")  
				        .comNumeroConvenio("1207113")  
				        .comCarteira("18")  
				        .comEndereco(enderecoBeneficiario)
				        .comNossoNumero(pedido.getBoleto().getNumero_boleto()); 

				Endereco enderecoPagador = Endereco.novoEndereco()
				        .comLogradouro(pedido.getEnderecoEntrega().getLogradouro())  
				        .comBairro(pedido.getEnderecoEntrega().getBairro())  
				        .comCep(pedido.getEnderecoEntrega().getCep())  
				        .comCidade(pedido.getEnderecoEntrega().getCidade())  
				        .comUf(pedido.getEnderecoEntrega().getEstado());  
				

				//Quem paga o boleto
				Pagador pagador = Pagador.novoPagador()  
				        .comNome(pedido.getPessoa().getNome())  
				        .comDocumento(pedido.getPessoa().getDocumento())
				        .comEndereco(enderecoPagador);

				Banco banco = new BancoDoBrasil();  

				Boleto boleto = Boleto.novoBoleto()  
				        .comBanco(banco)  
				        .comDatas(datas)  
				        .comBeneficiario(beneficiario)  
				        .comPagador(pagador)  
				        .comValorBoleto(pedido.getValorTotal())  
				        .comNumeroDoDocumento(pedido.getBoleto().getNumero_boleto())  
				        .comInstrucoes("Nao receber apos vencimento")  
				        .comLocaisDePagamento("Em qualquer agencia bancaria");  

				GeradorDeBoleto gerador = new GeradorDeBoleto(boleto);  
 
			    outputStream = new ByteArrayOutputStream();
				gerador.geraPDF(outputStream);
				
				// Para gerar um array de bytes a partir de um PDF  
				 bPDF =  outputStream.toByteArray();
				 bis = new ByteArrayInputStream(bPDF);
			} catch (Exception e) {
				e.printStackTrace();
				throw e;
			} finally {
			    if(null != outputStream) {
	                try { outputStream.close(); outputStream = null; }
	                catch(Exception ex) { }
	            }
			}
			
			return bis;
		}
		
 
}
