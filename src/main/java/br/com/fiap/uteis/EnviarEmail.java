package br.com.fiap.uteis;

import java.io.IOException;
import java.io.InputStream;

import javax.activation.DataSource;

import org.apache.commons.mail.ByteArrayDataSource;
import org.apache.commons.mail.Email;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.MultiPartEmail;
import org.apache.commons.mail.SimpleEmail;

import br.com.fiap.ecommerce.entity.Pedido;

public class EnviarEmail {
	private static final String HOST_NAME = "smtp.googlemail.com"; 
	private static final String EMAIL_FROM = "26scjfiap@gmail.com"; 
	private static final String EMAIL_PASSWORD = "fiap26scj";
	public static void enviarBoleto(Pedido pedido) throws IOException {
		
		try {  
	            MultiPartEmail email = new MultiPartEmail();
	            email.setDebug(true);
	            email.setHostName(HOST_NAME); // o servidor SMTP para envio do  e-mail
	 	        email.addTo(pedido.getPessoa().getUsuario().getLogin());
	 	      //  email.addTo("rockntiago@gmail.com");
	            email.setFrom(EMAIL_FROM, "Fiap E-Commerce");
	            email.setSubject("Boleto compra Fiap E-Commerce"); 
	            email.setMsg("Em anexo segue boleto referente a sua compra em Fiap E-Commerce \n");
	            email.setAuthentication(EMAIL_FROM,EMAIL_PASSWORD);
	            email.setSmtpPort(465);
	            email.setSSL(true); 
 
			
				InputStream is = GeradorBoletos.gerarBoletoPedido(pedido);
				DataSource source = new ByteArrayDataSource(is, "application/pdf");
				
				email.attach(source, "boleto.pdf", "Boleto");
				email.send();
		} catch (IOException e) { 
			e.printStackTrace(); 
		} catch (EmailException e) { 
			e.printStackTrace();
		}  
	} 
	
	public static void enviarEmailPedidoRegeitado(Pedido pedido) throws EmailException{
		SimpleEmail  email = new SimpleEmail();

		email.setDebug(true);
        email.setHostName(HOST_NAME); // o servidor SMTP para envio do  e-mail
        email.setAuthentication(EMAIL_FROM,EMAIL_PASSWORD);
        email.setSmtpPort(465);
        email.setFrom(EMAIL_FROM, "Fiap E-Commerce");
        email.addTo(pedido.getPessoa().getUsuario().getLogin());
        email.setSubject("Boleto compra Fiap E-Commerce"); 
        email.setMsg("Seu pedido foi rejeitado! entre em contato com nossa central de atendimento ao cliente.\n");
        email.setSSL(true); 
        email.send();
	}
	 
}
