package br.com.fiap.uteis;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class Uteis {

	public static Calendar adicionarDiasEmData(Date data, int dias){
		Calendar calendar = new GregorianCalendar();
		
		calendar.setTime(data);
		calendar.add(Calendar.DAY_OF_MONTH, dias);
		return calendar;
	}
	
 
}
