'use strict';

/**
 * @ngdoc overview
 * @name ecommerceWebHerokuApp
 * @description
 * # ecommerceWebHerokuApp
 *
 * Main module of the application.
 */
var app = angular
  .module('ecommerceWeb', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'ui.router',
    'ui.bootstrap',
    'ngMask',
    'naif.base64'
  ]);

app.config(function($stateProvider) {
  $stateProvider
  .state('home', { 
    url: '/',
    templateUrl: 'views/vitrine.html',
    controller: 'produtosController'
  })
  .state('login', { 
    url: '/login',
    templateUrl: 'views/login.html',
    controller: 'LoginController'
  })
  .state('quem_somos', { 
    url: '/quem_somos',
    templateUrl: 'views/quem_somos.html',
    controller: 'PageCtrl'
  })
  .state('produtos_loja', { 
    url: '/produtos',
    templateUrl: 'views/vitrine.html',
    controller: 'produtosController'
  })
  .state('detalhes_loja', { 
    url: '/produtos/detalhes/:codigoProduto',
    templateUrl: 'views/detalhes.html',
    controller: 'detalhesProdutoController'
  })
  .state('carrinho', { 
    url: '/carrinho',
    templateUrl: 'views/carrinho.html',
    controller: 'produtosController'
  }).state('produtos', { 
    url: '/admin/produtos',
    templateUrl: 'views/admin/produtos/produtos.html',
    controller: 'ProdutosAdminController'
  })
  .state('visualizarProduto', { 
    url: '/admin/produtos/:id/visualizar',
    templateUrl: 'views/admin/produtos/visualizar_produto.html',
    controller: 'ProdutoVisualizacaoController'
  })
  .state('cadastrarProduto', { 
    url: '/admin/produtos/cadastrar',
    templateUrl: 'views/admin/produtos/cadastro_produto.html',
    controller: 'ProdutoCadastroController'
  })
  .state('editarProduto', { 
    url: '/admin/produtos/:id/editar',
    templateUrl: 'views/admin/produtos/editar_produto.html',
    controller: 'ProdutoEditarController'
  })
  .state('pedido', { 
	    url: '/pedido',
	    templateUrl: 'views/cliente/pedido.html',
	    controller: 'pedidoController'
  })
  .state('pedidosCliente', { 
	    url: '/perfil/pedidos',
	    templateUrl: 'views/cliente/pedidos.html',
	    controller: 'PedidosClienteController'
  })
  .state('visualizarPedido', { 
	    url: '/pedido/:codigoPedido',
	    templateUrl: 'views/cliente/visualizar_pedido.html',
	    controller: 'PedidosVisualizacaoController'
  })
  .state('editarCliente', { 
    url: '/perfil/editar',
    templateUrl: 'views/cliente/editar_cliente.html',
    controller: 'PessoaEditarController'
  }); 
});

app.factory('LoginInterceptor', function ($window, $q, $injector) {
    return {
        request: function(config) {
            config.headers = config.headers || {};
            if ($window.localStorage.getItem('token')) {
              // may also use sessionStorage
              config.headers.Authorization = $window.localStorage.getItem('token');
            }
            return config || $q.when(config);
        },
        response: function(response) {
            if (response.status === 401) {
            	$injector.get('DataService').usuario.logout();
            	$injector.get('$state').go('login');
            }
            return response || $q.when(response);
        }
    };
});

// Register the previously created AuthInterceptor.
app.config(function ($httpProvider) {
    $httpProvider.interceptors.push('LoginInterceptor');
});

/***Only for Preview ***/
app.controller('MainController', ['$scope', function($scope) {
  $scope.templates =
    [ 
      { name: 'footer', url: 'views/templates/footer.html'},
     ];
  $scope.template = $scope.templates[0];

  $scope.headers =
    [
      { name: 'header', url: 'views/templates/header.html' },
     ];
  $scope.header = $scope.headers[0];

  $scope.baseURL = 'https://localhost:8081/ecommerce'
  $scope.baseURLDesenv = 'https://localhost:8081/ecommerce'
}]);

/**
 * Controls all other Pages
 */
app.controller('PageCtrl', function ( /*$scope, $location, $http */) {
});

app.config(['$httpProvider', function ($httpProvider) {
    $httpProvider.interceptors.push(function ($q, $rootScope) {
        if ($rootScope.activeCalls == undefined) {
            $rootScope.activeCalls = 0;
        }

        return {
            request: function (config) {
                $rootScope.activeCalls += 1;
                return config;
            },
            requestError: function (rejection) {
                $rootScope.activeCalls -= 1;
                return rejection;
            },
            response: function (response) {
                $rootScope.activeCalls -= 1;
                return response;
            },
            responseError: function (rejection) {
                $rootScope.activeCalls -= 1;
                return rejection;
            }
        };
    });
}]);

app.directive('loading', function () {
    return {
      restrict: 'E',
      replace:true,
      template: '<div class="loading"><img src="images/loading.gif"/></div>',
      link: function (scope, element, attrs) {

          scope.$watch('activeCalls', function (newVal, oldVal) {
              if (newVal == 0) {
                  $(element).hide();
              }
              else {
                  $(element).show();
              }
          });
      }
    }
});
