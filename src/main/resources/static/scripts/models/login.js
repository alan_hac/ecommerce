function Login(token, permissao, usuario){
    this.token = token;
    this.permissao = permissao;
    this.usuario = usuario;
}

function Usuario(){
	this.loginUsuario = {}
	
	//Expões as funções para quando criado novo usuário
	this.isADM = this.isADM;
	this.isCliente = this.isCliente 
	this.usuarioLogado = this.usuarioLogado 
	this.logout = this.logout 
	this.login = this.login
	this.atualizarUsuario = this.atualizarUsuario;
	
	this.usuarioAutenticado();
}

Usuario.prototype.atualizarUsuario = function(){
	if (localStorage != null && JSON != null) {
		console.log(this.loginUsuario)
        localStorage["ecommerce_login"] = JSON.stringify(this.loginUsuario);
		console.log(localStorage["ecommerce_login"])
	}
}

// Verifica se o usuário já está autenticado, pois será compartilhado na sessão
Usuario.prototype.usuarioAutenticado = function(){
	var login = localStorage != null ? localStorage["ecommerce_login"] : null;
    if (login != null && JSON != null) {
    	try {
    		var login = JSON.parse(login);
    		if (login.token != null && login.permissao != null) {
    			this.loginUsuario = new Login(login.token, login.permissao, login.usuario);
    		}
                
		} catch (err) {
		}
    }
        
}

Usuario.prototype.isADM = function(){
	return this.loginUsuario.permissao === 'ADMINISTRADOR';
	
}

Usuario.prototype.isCliente = function(){
	return this.loginUsuario.permissao === 'CLIENTE'
}

Usuario.prototype.usuarioLogado = function(){
	return this.loginUsuario.token != null;
}

// Remove o usuario logado do local storage
Usuario.prototype.logout = function(){
	this.loginUsuario = new Usuario();
    localStorage['token'] = null;
    localStorage["ecommerce_login"] = null;
};


//Joga o usuário logado no loca storage
Usuario.prototype.login = function(token, permissao, usuario){
	this.loginUsuario = new Login(token, permissao, usuario);
	
	if (localStorage != null && JSON != null) {
        localStorage["ecommerce_login"] = JSON.stringify(this.loginUsuario);
        localStorage['token'] = token;
    }
	
}