﻿//Objeto representando um produto no carrinho
function produto(idProduto, nome, foto, preco, quantidade) {
    this.idProduto = idProduto;
    this.nome = nome;
    this.foto = foto;
    this.preco = preco * 1;
    this.quantidade = quantidade * 1;
}

// Construtor do Carrinho
function Carrinho() {
    this.limparCarrinho = false;
    this.produtos = [];

    // load produtos from local storage when initializing
    this.carregarProdutos();

    // save items to local storage when unloading
    var self = this;
    $(window).unload(function () {
        if (self.limparCarrinho) {
            self.limparProdutos();
        }
        self.salvarProdutos();
        self.limparCarrinho = false;
    });
}

// Carrega os Produtos no localStorage
Carrinho.prototype.carregarProdutos = function () {
    var produtos = localStorage != null ? localStorage["ecommerce_produtos"] : null;
    if (produtos != null && JSON != null) {
        try {
            var produtos = JSON.parse(produtos);
            for (var i = 0; i < produtos.length; i++) {
                var item = produtos[i];
                if (item.idProduto != null && item.nome != null  && item.preco != null && item.quantidade != null) {
                    item = new produto(item.idProduto, item.nome, item.foto, item.preco, item.quantidade);
                    this.produtos.push(item);
                }
            }
        }
        catch (err) {
        }
    }
}

// Grava os itens no local storage
Carrinho.prototype.salvarProdutos = function () {
    if (localStorage != null && JSON != null) {
        localStorage["ecommerce_produtos"] = JSON.stringify(this.produtos);
    }
}

// Adiciona um produto no carrinho
Carrinho.prototype.adicionarProduto = function (idProduto, nome, foto, preco, quantidade) {
    quantidade = this.toNumber(quantidade);
    if (quantidade != 0) {

        // atualiza a quantidade do item
        var encontrado = false;
        for (var i = 0; i < this.produtos.length && !encontrado; i++) {
            var item = this.produtos[i];
            if (item.idProduto == idProduto) {
                encontrado = true;
                item.quantidade = this.toNumber(item.quantidade + quantidade);
                if (item.quantidade <= 0) {
                    this.produtos.splice(i, 1);
                }
            }
        }

        // Item novo, inserie ele
        if (!encontrado) {
            var item = new produto(idProduto, nome, foto, preco, quantidade);
            this.produtos.push(item);
        }

        // save as mudanças
        this.salvarProdutos();
    }
}

// Pega o Valor total do carrinho
Carrinho.prototype.getValorTotal = function (idProduto) {
    var total = 0;
    for (var i = 0; i < this.produtos.length; i++) {
        var item = this.produtos[i];
        if (idProduto == null || item.idProduto == idProduto) {
            total += this.toNumber(item.quantidade * item.preco);
        }
    }
    return total;
}

// Pega o total de itens no carrinho
Carrinho.prototype.getQuantidadeTotal = function (idProduto) {
    var count = 0;
    for (var i = 0; i < this.produtos.length; i++) {
        var item = this.produtos[i];
        if (idProduto == null || item.idProduto == idProduto) {
            count += this.toNumber(item.quantidade);
        }
    }
    return count;
}

// Limpa o Carrinho
Carrinho.prototype.esvaziarCarrinho = function () {
    this.produtos = [];
    this.salvarProdutos();
}

Carrinho.prototype.toNumber = function (value) {
    value = value * 1;
    return isNaN(value) ? 0 : value;
}
