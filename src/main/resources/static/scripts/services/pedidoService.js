'use strict';

/**
 * @ngdoc service
 * @name ecommerceWebHerokuApp.servicoProdutos
 * @description
 * # servicoProdutos
 * Service in the ecommerceWebHerokuApp.
 https://ecommerce-fiap-api.herokuapp.com/ecommerce/produtos
 */
app.service('PedidoService', function ($http, $q) {
     
      this.confirmarPedido = function (pedido) {
        return $http.post('/ecommerce/api/pedido/confirmar', pedido);
      };
      
      this.buscarPedido = function(pedido){
    	  return $http.get('/ecommerce/api/pedidos/'+ pedido);
      }
});
