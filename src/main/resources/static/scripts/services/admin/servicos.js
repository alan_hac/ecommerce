'use strict';

/**
 * @ngdoc service
 * @name ecommerceWebHerokuApp.servicoProdutos
 * @description
 * # servicoProdutos
 * Service in the ecommerceWebHerokuApp.
 */
app.factory('Produto',function($resource){
    return $resource('/ecommerce/api/produtos/:id',{id:'@_id'},{
    	'update': { method:'PUT' }
    });
})
.service('popupService',function($window){
    this.showPopup=function(message){
        return $window.confirm(message);
    }
});
