app.factory("DataService", function ($window,$http) {

    // create shopping cart
    var carrinho = new Carrinho("Ecommerce FIAP");
    var usuario = new Usuario();
   
    // return data object with store and cart
    return {
        carrinho: carrinho,
        usuario: usuario
    };
});
