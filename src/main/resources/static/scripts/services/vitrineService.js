'use strict';

/**
 * @ngdoc service
 * @name ecommerceWebHerokuApp.servicoProdutos
 * @description
 * # servicoProdutos
 * Service in the ecommerceWebHerokuApp.
 https://ecommerce-fiap-api.herokuapp.com/ecommerce/produtos
 */
app
  .service('ProdutoService', function ($http, $q) {
     
      this.buscarProdutos = function () {
        return $http.get('/ecommerce/vitrine/produtos');
      };
      
      this.buscarDetalhesProduto = function(codigoProduto) {
    	  return $http.get('/ecommerce/vitrine/produtos/' + codigoProduto);
      }
      
      this.buscarCategorias = function () {
          return $http.get('/ecommerce/vitrine/produtos/categorias');
      };
      
      this.validarEstoqueProdutos = function(produtos){
    	  return $http.post('/ecommerce/vitrine/produtos/validarEstoque', produtos);
      }
}).factory('ListaProdutos', function($http){
	 var produtos = {};

	 produtos.list = [];
	 
	 produtos.produtoExistente = {}
	 
	 produtos.add = function(produto){
		 produtos.list = produto;
	 };
	 
	 produtos.get = function(codigoProduto){
		$.each(produtos.list, function(index){
			var produto = produtos.list[index];
			if (produto.idProduto == codigoProduto){
				produtos.produtoExistente = produto;
			}
		}); 
	 };
	 return produtos;
});
