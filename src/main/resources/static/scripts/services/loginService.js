'use strict';

app.service('loginService', function($http) {
    return {
        login : function(usuario, senha) {
            return $http.post('/ecommerce/usuario/login', {"login": usuario, "senha": senha});
        },

        hasRole : function(role) {
            return $http.get('/ecommerce/api/usuario/perfil/' + role);
        }
    };
});