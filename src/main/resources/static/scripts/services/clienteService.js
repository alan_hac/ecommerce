'use strict';

/**
 * @ngdoc service
 * @name ecommerceWebHerokuApp.servicoCliente
 * @description
 * # servicoCliente
 * Service in the ecommerceWebHerokuApp.
 https://ecommerce-fiap-api.herokuapp.com/ecommerce/cliente
 */
app.service('ClienteService', function ($http, $q) {
     
      this.cadastrar = function (cliente) {
        return $http.post('/ecommerce/cliente/cadastrar', cliente);
      };
      
      
      this.buscarEnderecos = function() {
    	  return $http.get('/ecommerce/api/cliente/enderecos/');
      }
       
      this.editar = function(){
    	  return $http.get('/ecommerce/api/cliente/editar');    	  
      }
      
      this.buscarMeusPedidos = function(filtros){
    	  return $http.post('/ecommerce/api/pedidos', filtros);
      }
      
      this.cancelarPedido = function(pedido){
    	  return $http.post('/ecommerce/api/pedido/cancelar', pedido)
      }
});