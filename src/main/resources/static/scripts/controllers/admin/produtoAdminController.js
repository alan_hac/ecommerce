/**
 * @ngdoc ProdutosAdminController
 * @description
 * @author Paulo
 * Funcionalidades de CRUD de produto para usuário administrativo
 * 
 * onde é realizado o Two-Way-Data-Binding do angular através do Resource de Produto onde é um mock para as url's do CRUD para produto
 */
'use strict';
app.controller('ProdutosAdminController', function($scope, $state, popupService, $window, Produto) {
 
  $scope.produtos = Produto.query(); //fetch all

  $scope.remover = function(produto) { // Desativa
    if (popupService.showPopup('Certeza?')) {
      console.log(produto)
      produto.$delete({id : produto.idProduto},function() {
    	  $state.go('produtos'); 
      });
    }
  };
})
.controller('ProdutoVisualizacaoController', function($scope, $stateParams, Produto) {
  $scope.produto = Produto.get({ id: $stateParams.id });
})
.controller('ProdutoCadastroController', function($scope, $state, $stateParams, Produto, ProdutoService) {
  $scope.produto = new Produto(); 
  $scope.listaErros = [];
  
  ProdutoService.buscarCategorias().success(function(data){
  	$scope.categorias = data;
  });
  
  $scope.cadastrar = function() { 
	$scope.produto.foto = $scope.produto.foto.base64;
    $scope.produto.$save().then(function(data) {
    	console.log(data)
    	if(data.listaErros != null){
    		$scope.listaErros = data.listaErros;
    	} else {
    		$state.go('produtos');
    	}
    });
  };
})
.controller('ProdutoEditarController', function($scope, $state, $stateParams, Produto, ProdutoService) {
  $scope.atualizar = function() {
	$scope.produto.foto = $scope.produto.foto.base64;
    $scope.produto.$update(function(data) {
    	if(data.listaErros != null){
    		$scope.listaErros = data.listaErros;
    	} else {
    		$state.go('produtos');
    	} 
    });
  };
  
  ProdutoService.buscarCategorias().success(function(data){
  	$scope.categorias = data;
  });
  
  $scope.carregarEditar = function() {
    $scope.produto = Produto.get({ id: $stateParams.id });
  };

  $scope.carregarEditar();
});