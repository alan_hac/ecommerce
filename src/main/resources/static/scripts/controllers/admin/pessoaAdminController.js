'use strict';
app.controller('PessoaEditarController', function($scope, $state, $stateParams, Pessoa) {
  $scope.atualizar = function() {
    $scope.pessoa.$update(function() {
      $state.go('pessoas'); 
    });
  };

  $scope.carregarEditar = function() {
    $scope.pessoa = Pessoa.get({ id: $stateParams.id });
  };

  $scope.carregarEditar();
});