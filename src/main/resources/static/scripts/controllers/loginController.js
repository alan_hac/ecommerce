'use strict';

app.controller('LoginController', function ($window, $scope, $http, $state, loginService, DataService) {
    $scope.token = null;
    $scope.error = null;

    //Realiza a chamada para o serviço de login e coloca o token na sessão e no cabeçalho, e logo em seguida acessa o serviço de 
    $scope.login = function() {
        $scope.error = null;
        loginService.login($scope.usuario,$scope.senha).success(function(data){
        	console.log(data)
        	if(data.status === 'UNAUTHORIZED'){
        		$scope.message = data.listaErros
        		$state.go('login');
        	} else {
        		DataService.usuario.login(data.token);
        		$scope.checkRoles();
        		$state.go('home');
        	}
        });
    }

    $scope.checkRoles = function() {
        loginService.hasRole('CLIENTE').then(function(cliente) {
        	if (cliente) {
        		DataService.usuario.loginUsuario.permissao = 'CLIENTE';
        	}
            DataService.usuario.atualizarUsuario();
    	});
        loginService.hasRole('ADMINISTRADOR').success(function(admin) {
        	console.log(admin)
        	if(admin){
        		DataService.usuario.loginUsuario.permissao = 'ADMINISTRADOR';
        	}
            DataService.usuario.atualizarUsuario();
        });
    }

    //Limpa o usuario e token da sessão e remove o token de autorização do cabeçalho
    $scope.logout = function() {
    	DataService.usuario.logout();
    	$state.go('home');
    }

    //Cadastrar novo usuario apartir do login
    $scope.cadastrar = function() {
        return $scope.token != null;
    }
    
    $scope.IsAdmin = function(){
    	return DataService.usuario.isADM();
    }
    
    $scope.IsCliente = function(){
    	return DataService.usuario.isCliente();
    }
    
    $scope.isLogado = function(){
    	return DataService.usuario.usuarioLogado();
    }
    
});

var compareTo = function() {
    return {
        require: "ngModel",
        scope: {
            otherModelValue: "=compareTo"
        },
        link: function(scope, element, attributes, ngModel) {
             
            ngModel.$validators.compareTo = function(modelValue) {
                return modelValue == scope.otherModelValue;
            };
 
            scope.$watch("otherModelValue", function() {
                ngModel.$validate();
            });
        }
    };
};
