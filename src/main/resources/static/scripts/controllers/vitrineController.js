﻿'use strict';

app.controller('produtosController', function ($scope, $filter, ProdutoService, ListaProdutos, DataService, $routeParams) {

	$scope.pagedItems = [];
    $scope.categorias = [];
    $scope.carrinho = DataService.carrinho;

    ProdutoService.buscarProdutos().success(function(data){
    	$scope.pagedItems = data;
    	ListaProdutos.add(data)
    });
    
    ProdutoService.buscarCategorias().success(function(data){
    	$scope.categorias = data;
    });


   //Definições para paginação
    //Pagina atual
    $scope.filteredItems = [];

    $scope.currentPage = 0;
    //Quantidade de itens por pagina
    $scope.pageSize = 9;
    //Quantidade de paginas com base na quantidade de produtos
    $scope.numberOfPages = Math.ceil($scope.pagedItems.length / $scope.pageSize);

    //Método de pesquisa da caixa de texto
    $scope.pesquisa = function (nome) {
	        $scope.filteredItems = $filter('filter')($scope.pagedItems, function (product) {
	            for (var attr in product) {
	                var produto = product[attr]
	                if (searchMatch(produto[nome], $scope.query))
	                    return true;
	            }
	            return false;
	        });
	        $scope.currentPage = 0;
	        $scope.groupToPages();
    };

    // Filtro por categoria
    $scope.filtroCategoria = function (coluna, categoria) {
	        $scope.filteredItems = $filter('filter')(ListaProdutos.list, function (product) {
	        	for (var attr in product) {
	                if (searchMatch(product[coluna], categoria))
	                    return true;
	            }
	            return false;
	        });
	        $scope.currentPage = 0;
	        $scope.groupToPages();
    };
    $scope.groupToPages = function () {
        $scope.pagedItems = [];

        for (var i = 0; i < $scope.filteredItems.length; i++) {
        	if (i % $scope.pageSize === 0) {
                $scope.pagedItems[Math.floor(i / $scope.pageSize)] = $scope.filteredItems[i];
            } else {
                $scope.pagedItems.push($scope.filteredItems[i]);
            }
        }
    };
    
    var searchMatch = function (valor1, valor2) {
        if (!valor2) {
            return true;
        }
        console.log(valor1 + " = " + valor2)
        return valor1.toLowerCase().indexOf(valor2.toLowerCase()) !== -1;
    };
    
    $scope.possuiFoto = function(produto){
    	return produto.foto != null;
    }
    
    $scope.validarCarrinho = function(){
    	if ($scope.carrinho.produtos.length > 0) {
			ProdutoService.validarEstoqueProdutos($scope.carrinho.produtos).success(function(response){
				if(response){
					if(response.listaErros != null){
						$scope.listaErros = response.listaErros;
						$scope.carrinho.esvaziarCarrinho();
					}
				}
			})
		}
    }
    
    // functions have been describe process the data for display
    $scope.filtroCategoria();
    $scope.pesquisa();
    
    $scope.validarCarrinho();

});

app.controller('detalhesProdutoController', function ($scope, ProdutoService,DataService, $stateParams, ListaProdutos) {
    $scope.cart = DataService.cart;
    if ($stateParams.codigoProduto != null) {
    	ListaProdutos.get($stateParams.codigoProduto);
    	var produtoExistente = ListaProdutos.produtoExistente;
    	
    	if (Object.keys(produtoExistente).length === 0 ) {
    		ProdutoService.buscarDetalhesProduto($stateParams.codigoProduto).success(function(data){
    			$scope.product = data;
    		})
		} else {
			$scope.product =  produtoExistente;
		}
    }
});