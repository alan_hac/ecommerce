/**
 * @ngdoc pedidoController
 * @description
 * Funcionalidades de pedido
 * 
 * Funcionalidades da tela de pedido, onde tem a responsábilidade de disponibilizar as informações que estão na sessão de forma simples para que o usuário possa escolher o endereço
 */
'use strict';

app.controller('pedidoController', function ($scope, PedidoService, ClienteService, DataService, $state) {

	$scope.usuario = DataService.usuario;
	$scope.cadastrarNovoEndereco = false;
	if(isEmpty($scope.usuario)){
		$state.go('login')
	}
	
	$scope.carrinho = DataService.carrinho;
	
	/**
	 * Cria uma nova propriedade do escopo da pagina para os endereços, e logo em seguida chama função para 
	 * buscar via serviço os endereços
	 */
	$scope.enderecosUsuario = {};
	$scope.novoEndereco = {};

	$scope.pedido = {
		enderecoEntrega: {},
		produtos: [],
		valorTotal: $scope.carrinho.getValorTotal()
	};
	
	//Remove foto para ficar mais clean o json
	angular.forEach(DataService.carrinho.produtos, function(value, key) {
		var produto = {
				idProduto: value.idProduto,
				preco: value.preco,
				quantidade: value.quantidade
		}
		$scope.pedido.produtos.push(produto);
	});
	
	/**
	 *  Método para realizar a chamada para o serviço de confirmação de pedido, que cria inicialmenteo  
	 *  pedido e da baixa no estoque já reservando o produto para o cliente
	 */
	$scope.confirmarPedido = function(){
		PedidoService.confirmarPedido($scope.pedido).success(function(data){
			if(data.listaErros != null){
	    		$scope.listaErros = data.listaErros;
	    	} else {
	    		$scope.pedido = {};
	    		$scope.enderecosUsuario = {};
	    		$scope.novoEndereco = {};
	    		DataService.carrinho.esvaziarCarrinho();
	    		$state.go('visualizarPedido',{codigoPedido: data.idPedido})
	    	}
		});
	}
	
	$scope.buscarEnderecos = function(){
		ClienteService.buscarEnderecos()
		.success(function(data){
			$scope.enderecosUsuario = data;
		}).error(function(error){
			alert("Não foi possivel obter os endereços cadastrados. Insira um novo ou tente novamente mais tarde")
		});
	}
	
	$scope.novo = function(){
		$scope.cadastrarNovoEndereco = true;
	}
	$scope.cancel = function(){
		$scope.cadastrarNovoEndereco = false;
	}
	
	$scope.cadastrarEndereco = function(){
		$scope.enderecosUsuario.push($scope.novoEndereco);
		$scope.cadastrarNovoEndereco = false;
		$scope.novoEndereco = {};
	}
	  
    $scope.possuiFoto = function(produto){
    	return produto.foto != null;
    }

	$scope.buscarEnderecos();
	
//	só se for a pagina de carrinho $scope.validarEstoque();
}).controller('PedidosVisualizacaoController', function($scope, PedidoService, $stateParams){
	PedidoService.buscarPedido($stateParams.codigoPedido).success(function(data){
		$scope.pedido = data;
	});
});


function isEmpty(obj) {
    return Object.keys(obj).length === 0;
}