﻿'use strict';
app.controller('ClienteController', function($scope, $state, ClienteService) {
	 $scope.pessoa = {}; 

	  $scope.cadastrar = function() { 
	    ClienteService.cadastrar($scope.pessoa	).success(function(data){
	    	console.log(data);
	    	$state.go("home");
	    });
	  }; 
	  
}).controller('PessoaEditarController', function($scope, ClienteService, $stateParams) {

	 $scope.salvar = function() { 
		 ClienteService.cadastrar($scope.cliente);
	 }
	 $scope.editar = function() { 
	   		$scope.cliente = ClienteService.editar().success(function(data){
	   		$scope.cliente = data;   
	   		$scope.enderecos = $scope.cliente.enderecos;
	   	});
    }

	$scope.editar();

	$scope.novoEndereco = function(){
		$scope.cadastrarNovoEndereco = true;
	}
	$scope.cancel = function(){
		$scope.cadastrarNovoEndereco = false;
	}
	
	$scope.containsObject = function(obj, list) {
	    var i;
	    for (i = 0; i < list.length; i++) {
	        if (angular.equals(list[i], obj)) {
	            return true;
	        }
	    }

	    return false;
	};
	
	$scope.cadastrarEndereco = function(){
		$scope.endereco = $scope.enderecoNovo;
		if (!$scope.containsObject($scope.endereco, $scope.cliente.enderecos)){
			$scope.cliente.enderecos.push($scope.endereco);
			$scope.enderecos = $scope.cliente.enderecos;
		}
		$scope.cadastrarNovoEndereco = false;
		$scope.enderecoNovo = {};
	}

	$scope.editarEndereco = function(data){
		$scope.cadastrarNovoEndereco  = true;
		$scope.enderecoNovo = data;
	}
}).controller('PedidosClienteController', function($scope, ClienteService, DataService, $state){

	$scope.usuario = DataService.usuario;
	
	if(!$scope.usuario.usuarioLogado()){
		$state.go('login')
	}
	
	$scope.filtros = {
			
	}
	
	$scope.buscarPedidos = function(){
		ClienteService.buscarMeusPedidos($scope.filtros).success(function(data){
			$scope.meusPedidos = data;
		});
	}

	$scope.cancelarPedido = function(pedido){
		ClienteService.cancelarPedido(pedido.idPedido).success(function(data){
			var index = $scope.meusPedidos.indexOf(pedido);
			$scope.meusPedidos.splice(pedido, 1); 
		});
	}
	
	$scope.validaCancelamentoPedido = function(pedido){
		return pedido.situacao != 'SOLICITADO';
	}
	$scope.validaPagamentoPedido = function(pedido){
		return pedido.situacao != 'SOLICITADO';
	}
	
	$scope.buscarPedidos();
}); 
 
